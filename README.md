### `npm install`

To install and setup the project.

### `npm start`

Runs the app in the development mode.


###Login Test Accounts: 

Administrator:  admin - admin

Users:  katerina - katerina
        natasa - natasa
        thodoris - thodoris

###Mock Services Used:
getBadges: http://www.mocky.io/v2/5d4d1ad63300004c00337540
getUsers: http://www.mocky.io/v2/5d5c04f63200000f00628cae
getPlatformMode: http://www.mocky.io/v2/5d5bfacb3200005100628c6b
getUserDetails: thodoris - http://www.mocky.io/v2/5d5d00733300004f0057b5f0
                katerina - http://www.mocky.io/v2/5d5d0111330000610057b5f3
                natasa - http://www.mocky.io/v2/5d5d01533300002a0057b5f7

### Built using

- [Chart.js](https://www.chartjs.org/)
- [Flux](https://facebook.github.io/flux/)
- [No UI Slider](https://refreshless.com/nouislider/)
- [React Datepicker](https://www.npmjs.com/package/react-datepicker)
- [Quill](https://quilljs.com/)

<br />

