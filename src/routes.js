import React from "react";
import {
  Redirect
} from "react-router-dom";

// Layout Types
import {
  DefaultLayout,
  LoginLayout,
  ErrorLayout
} from "./layouts";

// Route Views
import BlogOverview from "./views/BlogOverview";
import UserProfileLite from "./views/UserProfileLite";
import GeneralConfiguration from "./views/GeneralConfiguration";
import AddNewRequest from "./views/AddNewRequest";
import AddNewProposal from "./views/AddNewProposal";
import LoginPage from "./views/LoginPage";
import ComponentsOverview from "./views/ComponentsOverview";
import Tables from "./views/Tables";
import RequestsToSponsor from "./views/RequestsToSponsor";
import MyRequests from "./views/MyRequests";
import GiveCoins from "./views/GiveCoins";
import BlogPosts from "./views/BlogPosts";
import FeedCrowdfunding from "./views/FeedCrowdfunding";
import Errors from "./views/Errors";
import Store from "./views/Store";
import Home from "./views/Home";

// import { generateKeyPair } from "crypto";

export default [{
  path: "/",
  exact: true,
  layout: DefaultLayout,
  component: () => < Redirect to="/login" />
},
{
  path: "/blog-overview",
  layout: localStorage.getItem('user') ? DefaultLayout : ErrorLayout,
  component: localStorage.getItem('user') ? BlogOverview : Errors,
},
{
  path: "/store",
  layout: DefaultLayout,
  component: localStorage.getItem('role') && localStorage.getItem('role') ===
    'normal' ? Store : Errors,
},
{
  path: "/success-feed",
  layout: DefaultLayout,
  component: localStorage.getItem('role') && localStorage.getItem('role') ===
    'normal' ? Home : Errors,
},
{
  path: "/general-configuration",
  layout: localStorage.getItem('user') ? DefaultLayout : ErrorLayout,
  component: localStorage.getItem('user') && localStorage.getItem('user') ===
    'admin' ? GeneralConfiguration : Errors,
},
{
  path: "/users-profiles",
  layout: localStorage.getItem('user') ? DefaultLayout : ErrorLayout,
  component: localStorage.getItem('user') && localStorage.getItem('user') ===
    'admin' ? UserProfileLite : Errors,
},
{
  path: "/add-request",
  layout: DefaultLayout,
  component: localStorage.getItem('role') && localStorage.getItem('role') ===
    'normal' ? AddNewRequest : Errors,
},
{
  path: "/my-requests",
  layout: DefaultLayout,
  component: localStorage.getItem('role') && localStorage.getItem('role') ===
    'normal' ? MyRequests : Errors,
},
{
  path: "/vote-request",
  layout: DefaultLayout,
  component: localStorage.getItem('role') && localStorage.getItem('role') ===
    'sponsor' ? RequestsToSponsor : Errors,
},
{
  path: "/give-coins",
  layout: DefaultLayout,
  component: localStorage.getItem('user') && localStorage.getItem('user') !==
    'admin' ? GiveCoins : Errors,
},
{
  path: "/feed",
  layout: DefaultLayout,
  component: FeedCrowdfunding,
},
{
  path: "/create-proposal",
  layout: DefaultLayout,
  component: AddNewProposal,
},
{
  path: "/login",
  layout: LoginLayout,
  component: LoginPage
},
{
  path: "/components-overview",
  layout: DefaultLayout,
  component: ComponentsOverview
},
{
  path: "/tables",
  layout: DefaultLayout,
  component: Tables
},
{
  path: "/blog-posts",
  layout: DefaultLayout,
  component: BlogPosts
}
];
