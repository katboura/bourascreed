import React from "react";
import { Container, Row, Col, FormSelect } from "shards-react";

import UserDetailsCard from "../components/user-profile-lite/UserDetailsCard";
import UserAccountDetailsCard from "../components/user-profile-lite/UserAccountDetailsCard";


export default class UserProfileLite extends React.Component {
  constructor(props) {
    super(props);

    this.state = {details: {}};
  }

  componentDidMount() {
      this.setState({ details: this.props.userSelectedFromParent })
  }

  componentDidUpdate(oldProps) {
    const newProps = this.props
    if(oldProps.userSelectedFromParent !== newProps.userSelectedFromParent) {
      this.setState({ details: newProps.userSelectedFromParent });
    }
  }

  render() {
      return (
      <Container fluid className="main-content-container px-4">
        {this.state.details
        ?
        <Row style={{padding:'10px'}}>
          <Col lg="4">
            <UserDetailsCard user={this.state.details}/>
          </Col>
          <Col lg="8">
            <UserAccountDetailsCard user={this.state.details}/>
          </Col>
        </Row>
        : null}
      </Container>
    )
  }
}