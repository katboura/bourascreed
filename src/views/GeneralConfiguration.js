import React from "react";
import { Container, Row, Col } from "shards-react";

import PageTitle from "../components/common/PageTitle";
import GeneralInfo from "../components/user-profile-lite/GeneralInfo";
import PlatformMode from "../components/user-profile-lite/PlatformMode";
import CoinsInfo from "../components/user-profile-lite/CoinsInfo";

export default class UserProfileLite extends React.Component {
  render() {
      return (
      <Container fluid className="main-content-container px-4">
        <Row noGutters className="page-header py-4">
          <PageTitle title="General Configuration" subtitle="Overview" md="12" className="ml-sm-auto mr-sm-auto" />
        </Row>

        <Row>
          <Col lg="11">
            <PlatformMode/>
          </Col>
        </Row>

        <Row>
          <Col lg="11">
            <GeneralInfo/>
          </Col>
        </Row>
        <Row>
          <Col lg="11">
            <CoinsInfo/>
          </Col>
        </Row>
      </Container>
    )
  }
}