import React from "react";
import { Container, Row, Col, Card, CardFooter, Badge, CardBody, Button, InputGroup, FormInput } from "shards-react";

import PageTitle from "../components/common/PageTitle";
import user1 from './../images/avatars/1.jpg';
import user2 from './../images/avatars/2.jpg';
import user0 from './../images/avatars/0.jpg';
import user3 from './../images/avatars/4.jpg';
import axios from 'axios';

export default class UserAccountDetails extends React.Component {


constructor(props) {
    super(props);

    this.state = {requests: ''};
  }

  componentDidMount() {

  axios.get('http://localhost:8080/awards/request/all').then(res => {
    console.log(res.data)
    this.setState({requests: res.data});
  }).catch(console.log)  
  }

  vote(requestId){
    let userDetails = JSON.parse(localStorage.getItem('userDetails'));
    let voteCoins = document.getElementById('vote#'+requestId).value;

    let data = this.state.requests;
    for (var i = 0; i < data.length; i++) {
      if(data[i].requestId === requestId) {
        if (voteCoins){
            data[i].points = data[i].points +  parseInt(voteCoins);
          if (data[i].points >= data[i].maxPointsNeeded  && data[i].status === 1) {
            data[i].status = 2;
          }
          this.setState({requests: data});
        }        
      }
    }

    //call service to sponsor    
    axios.post(`http://localhost:8080/awards/request/sponsor/`+userDetails.userId+'/'+requestId+'?points='+parseInt(voteCoins)).then((res) => {
      document.getElementById('vote#'+requestId).value = "";
        axios.get('http://localhost:8080/awards/user/'+userDetails.userId).then(res => {
          let details = res.data;
          localStorage.setItem('userDetails', JSON.stringify(details));
          this.setState({user: details});
        }).catch(console.log)
      }).catch(function(error){
        console.log(error)
      })
    

    }

  getImageForUser(userName){
    let image;
      if(userName === 'katerina'){
        image = user2;
      } else if (userName === 'natasa') {
        image = user0;
      }else if (userName === 'george') {
        image = user3;
      } else{
        image = user1;
      }
      return image;
  }

  render() {
    //filter through requests to get the pending requests only
    this.requests = []
    if (this.state.requests && this.state.requests.length>0){
      this.requests = this.state.requests.filter(( function (request, id) {
        return request.status === 1;
      }));
    }

  return (
    <Container fluid className="main-content-container px-4">
    {/* Page Header */}
    <Row noGutters className="page-header py-4">
      <PageTitle sm="4" title="Ideas" subtitle="Ideas feed" className="text-sm-left" />
    </Row>
  
    {/* Third Row of Posts */}
    <Row>
      {this.requests && this.requests.length>0 ? this.requests.map((post, idx) => (
        <Col lg="4" key={idx}>
          <Card small className="card-post mb-4">
            <CardBody>
              <h5 className="card-title">{post.title}</h5>
              <p style={{height: '60px'}} className="card-text text-muted">{post.body}</p>
              <Badge
                pill
                className={`card-post__category bg-${post.category_theme}`}
                >
                {post.category}
              </Badge>
              <div style={{paddingLeft: '5px',fontSize: '12px', paddingTop:"10px"}}>Status: <span style={{color:'green'}}>pending sponsor</span></div>
              <div style={{paddingTop: '7px',paddingLeft: '5px',fontSize: '12px'}}>Maximum points needed: <span style={{color:'purple'}}>{post.maxPointsNeeded}</span></div>
              <div style={{paddingTop: '7px',paddingLeft: '5px',fontSize: '12px'}}>Points gathered: {post.points}</div>
            </CardBody>
            <CardFooter className="border-top d-flex">
            {post.user ?
              <div className="card-post__author d-flex">       
                  <img
                alt={post.user.username}
                href="#"
                className="card-post__author-avatar card-post__author-avatar--small"
                src={this.getImageForUser(post.user.username)}
                />
               <span style={{paddingTop: '7px',paddingLeft: '5px',fontSize: '12px'}}>Proposed by {post.user.firstName +' '+ post.user.lastName}</span>
                <div className="d-flex flex-column justify-content-center ml-3">
                  <span className="card-post__author-name">
                    {post.author}
                  </span>
                  <small className="text-muted">{post.date}</small>
                </div>
                </div>
                  : null
                  }
              
              
              <div className="my-auto ml-auto">
                <InputGroup className="ml-auto">
                  <FormInput placeholder="Vote" type="number" id={'vote#'+post.requestId} />
                    <Button theme="white" className="px-2" onClick={() => this.vote(post.requestId)}>
                      <i className="material-icons">add</i>
                    </Button>
                </InputGroup>
            </div>
            </CardFooter>
          </Card>
        </Col>
      )
      ) : <div>No sponsor pending ideas to vote for.</div> }
    </Row>
  </Container>
  // <Container fluid className="main-content-container px-4">
  //   {/* Page Header */}
  //   <Row noGutters className="page-header py-4">
  //     <PageTitle sm="4" title="Vote for requests" subtitle="Requests" className="text-sm-left" />
  //   </Row>

  //  {/* Users +1 Requests Table */}
  //  <Row>
  //   <Col>
  //     <Card small className="mb-4">
  //       <CardHeader className="border-bottom">
  //         <h6 className="m-0">+1 Pending Requests</h6>
  //       </CardHeader>
  //       <CardBody className="p-0 pb-3">
  //       {this.requests.length > 0 ? 
  //         <table className="table mb-0">
  //           <thead className="bg-light">
  //             <tr>
  //               <th scope="col" className="border-0">
  //                 User
  //               </th>
  //               <th scope="col" className="border-0">
  //                 Request
  //               </th>
  //               <th scope="col" className="border-0">
  //                 Current Rating
  //               </th>
  //               <th scope="col" className="border-0">
  //                 Current Status
  //               </th>
  //               <th scope="col" className="border-0">
  //               </th>
  //             </tr>
  //           </thead>
  //           <tbody>
  //             {this.requests}
  //           </tbody>
  //         </table>
  //         : 
  //         <div className="p-3 list-group-item">No user requests available</div>}
  //       </CardBody>
  //     </Card>
  //   </Col>
  // </Row>
  // {/* End of Table */}
  // </Container>
)
}
};
