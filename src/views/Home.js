/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import {
    Container,
    Row,
    Col,
    Card,
    CardBody,
    CardFooter,
    Badge,
    Button,
    CardHeader,
    ListGroup,
    ListGroupItem,
    FormCheckbox,
    FormInput,
    Form,
    FormTextarea,
    FormGroup,
    InputGroup,
    FormSelect



} from "shards-react";

import PageTitle from "../components/common/PageTitle";

class BlogPosts extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            // First list of posts.
            PostsListOne: [
                {
                    backgroundImage: require("../images/content-management/1.jpeg"),
                    category: "Learning",
                    categoryTheme: "dark",
                    author: "Anna Kunis",
                    authorAvatar: require("../images/avatars/1.jpg"),
                    title: "Course Finished!",
                    body:
                        "Just finished an online course on React JS!              ",
                    date: "5 September 2019"
                },
                {
                    backgroundImage: require("../images/content-management/2.jpeg"),
                    category: "Atos Green",
                    categoryTheme: "info",
                    author: "James Jamerson",
                    authorAvatar: require("../images/avatars/2.jpg"),
                    title: "One Month Green",
                    body:
                        "Just completed my first month coming to Atos by bicycle every day!",
                    date: "29 August 2019"
                },
                {
                    backgroundImage: require("../images/content-management/3.jpeg"),
                    category: "WellBeing",
                    categoryTheme: "royal-blue",
                    author: "Jimmy Jackson",
                    authorAvatar: require("../images/avatars/2.jpg"),
                    title: "Second Month Fit!",
                    body:
                        "Just completed my second month at Yava and i feel fantastic!!",
                    date: "29 July 2019"
                },
                {
                    backgroundImage: require("../images/content-management/4.jpeg"),
                    category: "Innovation",
                    categoryTheme: "warning",
                    author: "John James",
                    authorAvatar: require("../images/avatars/3.jpg"),
                    title: "Vertical Health",
                    body:
                        "Just presented a Vertical Idea on the Health Sector!Wish me luck!",
                    date: "4 August 2019"
                }
            ],

        };
    }

    render() {
        const {
            PostsListOne,
            PostsListTwo,
            PostsListThree,
            PostsListFour
        } = this.state;

        return (
            <Container fluid className="main-content-container px-4">
                <Row>
                    {/* Editor */}
                    <Col lg="12" md="12">
                        <Card small className="mb-3">
                            <CardBody>
                                <Form className="add-new-post">

                                    <div style={{ 'fontSize': '18px', fontWeight: '300', marginBottom: '20px' }}>What would you like to share?</div>
                                    {/* <ReactQuill id="editorValue" className="add-new-post__editor mb-1" /> */}
                                    <FormGroup>
                                        <FormInput id="titleValue" placeholder="" />
                                    </FormGroup>
                                    <Button theme="accent" onClick="" >Post</Button>

                                </Form>

                            </CardBody>
                        </Card>
                    </Col>
                </Row>


                {/* Page Header */}
                <Row noGutters className="page-header py-4">
                    <PageTitle sm="4" title="Latest News" subtitle="" className="text-sm-left" />
                </Row>

                {/* First Row of Posts */}
                <Row>
                    {PostsListOne.map((post, idx) => (
                        <Col lg="3" md="6" sm="12" className="mb-4" key={idx}>
                            <Card small className="card-post card-post--1">

                                <div
                                    className="card-post__image"
                                    style={{ backgroundImage: `url(${post.backgroundImage})` }}
                                >
                                    <Badge
                                        pill
                                        className={`card-post__category bg-${post.categoryTheme}`}
                                    >
                                        {post.category}

                                    </Badge>
                                    <div className="card-post__author d-flex">
                                        <a
                                            href="#"
                                            className="card-post__author-avatar card-post__author-avatar--small"
                                            style={{ backgroundImage: `url('${post.authorAvatar}')` }}
                                        >
                                            Written by {post.author}
                                        </a>
                                    </div>
                                </div>

                                <CardBody>
                                    <h5 className="card-title">
                                        <a href="#" className="text-fiord-blue">
                                            {post.title}
                                        </a>
                                    </h5>
                                    <p className="card-text d-inline-block mb-3">{post.body}</p>
                                    <span className="text-muted">{post.date}</span>

                                </CardBody>
                                <Button theme="accent" onClick="" >Give Away Coins</Button>
                                <CardFooter>
                                    <button type="button" id="likebut" class="btn btn-white btn-sm">
                                        <i class="far fa-thumbs-up mr-1">

                                        </i>
                                    </button>




                                    <button type="button" id="likebut" class="btn btn-white btn-sm">
                                        Comment
                                    </button>




                                </CardFooter>
                            </Card>
                        </Col>
                    ))}
                </Row>

                {/* Second Row of Posts */}


                <Row>
                    {PostsListOne.map((post, idx) => (
                        <Col lg="3" md="6" sm="12" className="mb-4" key={idx}>
                            <Card small className="card-post card-post--1">
                                <div
                                    className="card-post__image"
                                    style={{ backgroundImage: `url(${post.backgroundImage})` }}
                                >
                                    <Badge
                                        pill
                                        className={`card-post__category bg-${post.categoryTheme}`}
                                    >
                                        {post.category}
                                    </Badge>
                                    <div className="card-post__author d-flex">
                                        <a
                                            href="#"
                                            className="card-post__author-avatar card-post__author-avatar--small"
                                            style={{ backgroundImage: `url('${post.authorAvatar}')` }}
                                        >
                                            Written by {post.author}
                                        </a>
                                    </div>
                                </div>
                                <CardBody>
                                    <h5 className="card-title">
                                        <a href="#" className="text-fiord-blue">
                                            {post.title}
                                        </a>
                                    </h5>
                                    <p className="card-text d-inline-block mb-3">{post.body}</p>
                                    <span className="text-muted">{post.date}</span>
                                </CardBody>
                                <Button theme="accent" onClick="" >Give Away Coins</Button>
                                <CardFooter>
                                    <button type="button" id="likebut" class="btn btn-white btn-sm">
                                        <i class="far fa-thumbs-up mr-1">

                                        </i>
                                    </button>


                                </CardFooter>
                            </Card>
                        </Col>
                    ))}
                </Row>

                {/* Third Row of Posts */}

                <Row>
                    {PostsListOne.map((post, idx) => (
                        <Col lg="3" md="6" sm="12" className="mb-4" key={idx}>
                            <Card small className="card-post card-post--1">
                                <div
                                    className="card-post__image"
                                    style={{ backgroundImage: `url(${post.backgroundImage})` }}
                                >
                                    <Badge
                                        pill
                                        className={`card-post__category bg-${post.categoryTheme}`}
                                    >
                                        {post.category}
                                    </Badge>
                                    <div className="card-post__author d-flex">
                                        <a
                                            href="#"
                                            className="card-post__author-avatar card-post__author-avatar--small"
                                            style={{ backgroundImage: `url('${post.authorAvatar}')` }}
                                        >
                                            Written by {post.author}
                                        </a>
                                    </div>
                                </div>
                                <CardBody>
                                    <h5 className="card-title">
                                        <a href="#" className="text-fiord-blue">
                                            {post.title}
                                        </a>
                                    </h5>
                                    <p className="card-text d-inline-block mb-3">{post.body}</p>
                                    <span className="text-muted">{post.date}</span>
                                </CardBody>
                                <Button theme="accent" onClick="" >Give Away Coins</Button>
                                <CardFooter>
                                    <button type="button" id="likebut" class="btn btn-white btn-sm">
                                        <i class="far fa-thumbs-up mr-1">

                                        </i>
                                    </button>


                                </CardFooter>
                            </Card>
                        </Col>
                    ))}
                </Row>

            </Container>
        );
    }
}

export default BlogPosts;
