/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardFooter,
  Badge,
  Button
} from "shards-react";

import PageTitle from "../components/common/PageTitle";

class BlogPosts extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // Third list of posts.
      PostsListThree: [
        {
          author: "Theo Bouras",
          authorAvatar: require("../images/avatars/1.jpg"),
          title: "Create a techonology community.",
          body:
            "Create a womens community and talk about technology in Atos.",
          date: "20 February 2019",
          category: "Technology",
          categoryTheme: "dark",
        },
        {
          author: "Katerina Boura",
          authorAvatar: require("../images/avatars/2.jpg"),
          title: "Travel in Paris for conference TechFreaks.",
          body:
            "Lets make a trip to Paris. Not only for travelling but also for learning and socializong with other technology Freaks! Lets go to conference TechFreaks!",
          date: "29 January 2019",
          category: "Travel",
          categoryTheme: "info",
        },
        {
          author: "John James",
          authorAvatar: require("../images/avatars/3.jpg"),
          title:
            "Volunteer in Parnitha mountain.",
          body:
            "Volunteer tree planting in Parnitha mountain. Go with colleagues to the recently burnt area and replant.",
          date: "10 March 2019",
          category: "Volunteer",
          categoryTheme: "royal-blue",
        },
        {
          author: "George Kastanas",
          authorAvatar: require("../images/avatars/4.jpg"),
          title: "Let's go to Brusells for the Marathon",
          body:
            "Let's g to Brussels for the Marathon! It is a great opportunity to meet the colleagues in Brussels and also run for a good cause.",
          date: "2 April 2019",
          category: "Travel",
          categoryTheme: "danger",
        },
        {
          author: "John James",
          authorAvatar: require("../images/avatars/3.jpg"),
          title:
            "Beach cleaning Volunteer",
          body:
            "Kavouri Beach cleaning volunteer work. Lets all got on Saturday to Kavouri beach and clean it from plastics. Then lets recycle them.",
          date: "21 February 2019",
          category: "Volunteer",
          categoryTheme: "warning",
        }
      ]
    };
  }

  render() {
    const { PostsListThree } = this.state;

    return (
      <Container fluid className="main-content-container px-4">
        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <PageTitle sm="4" title="Ideas" subtitle="Ideas feed" className="text-sm-left" />
        </Row>

        {/* Third Row of Posts */}
        <Row>
          {PostsListThree.map((post, idx) => (
            <Col lg="4" key={idx}>
              <Card small className="card-post mb-4">
                <CardBody style={{height:'300px'}}>
                  <h5 className="card-title">{post.title}</h5>
                  <p className="card-text text-muted">{post.body}</p>
                  <Badge
                    pill
                    className={`card-post__category bg-${post.categoryTheme}`}
                    >
                    {post.category}
                  </Badge>
                </CardBody>
                <CardFooter className="border-top d-flex">
                  <div className="card-post__author d-flex">
                    <a
                      href="#"
                      className="card-post__author-avatar card-post__author-avatar--small"
                      style={{ backgroundImage: `url('${post.authorAvatar}')` }}
                    >
                      Written by James Khan
                    </a>
                    <div className="d-flex flex-column justify-content-center ml-3">
                      <span className="card-post__author-name">
                        {post.author}
                      </span>
                      <small className="text-muted">{post.date}</small>
                    </div>
                  </div>
                  <div className="my-auto ml-auto">
                    <Button size="sm" theme="white">
                      <i className="far fa-thumbs-up mr-1" /> Like
                    </Button>
                    <div>
                      <Button size="sm" theme="white" style={{marginTop:'5px'}}>
                        <i className="far fa-address-book mr-1" aria-hidden="true"/> Give coins
                      </Button>
                    </div>
                  </div>
                </CardFooter>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
    );
  }
}

export default BlogPosts;
