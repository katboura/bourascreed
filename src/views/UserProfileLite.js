import React from "react";
import { Container, Row, Col, FormSelect } from "shards-react";

import PageTitle from "../components/common/PageTitle";
import UserDetails from "../components/user-profile-lite/UserDetails";
import UserAccountDetails from "../components/user-profile-lite/UserAccountDetails";
import user1image from '../images/avatars/0.jpg';
import user2image from '../images/avatars/1.jpg';
import user3image from '../images/avatars/2.jpg';
import user4image from '../images/avatars/4.jpg';

export default class UserProfileLite extends React.Component {
  constructor(props) {
    super(props);

    this.state = {value: 'nouser'};
  }

  handleOnChange = (event) => {
    
      this.setState({value: event.target.value}, function () {

        let users = this.state.details;
        for (var i = 0; i < users.length; i++) {
          if(users[i].user === this.state.value){
              let image;
              if(this.state.value === 'user2'){
                image = user3image;
              } else if (this.state.value === 'user1') {
                image = user2image;
              }else if (this.state.value === 'user4') {
                image = user4image;
              } else{
                image = user1image;
              }

              this.setState({avatar: image,
                            jobTitle: users[i].jobTitle,
                            user: users[i].user,
                            metaTitle: users[i].metaTitle,
                            metaValue: users[i].metaValue,
                            className: users[i].className,
                            totalCoinsFromOthers: users[i].totalCoinsFromOthers,
                            totalCoinsToGive: users[i].totalCoinsToGive,
                            name:users[i].name,
                            role:users[i].role,
                            accessToWallet:users[i].accessToWallet,
                            badgeOfuser:users[i].badgeName,
                            requests: users[i].requests,
                            coinsAvailablePlusOne: users[1].coinsAvailablePlusOne
                            })            
          }
        }
      });
    }

  componentDidMount() {
    fetch('http://www.mocky.io/v2/5d725ec82f000054007d4c7c')
    .then(res => res.json())
    .then((data) => {
      this.setState({ details: data })
    })
    .catch(console.log)
  }

  componentWillUnmount(){
    this.setState({})
  }
  
  render() {

    if (this.state.details){
      this.users = this.state.details.map((item) =>
      <option value={item.user} key={item.user}>{item.name}</option>
      );
    }  

      return (
      <Container fluid className="main-content-container px-4">
        <Row noGutters className="page-header py-4">
          <PageTitle title="User Configuration" md="12" className="ml-sm-auto mr-sm-auto" />
        </Row>
          <Col lg="4">
            <Row noGutters className="page-header py-2">
            Select a user to configure: 
            </Row>
            <FormSelect
            size="sm"
            value={this.state.value}
            style={{ maxWidth: "230px" }}
            onChange={this.handleOnChange}>
                <option value="nouser">Select a user</option>
                {this.users}
              </FormSelect>
            </Col>
        <Row>
          <br/>

        </Row>
        <Row>
          <Col lg="4">
            <UserDetails user={this.state}/>
          </Col>
          <Col lg="8">
            <UserAccountDetails user={this.state}/>
          </Col>
        </Row>
      </Container>
    )
  }
}