import React from "react";
import {
  Alert
} from "shards-react";
import coinsGiven from './../images/coinsToGive.png';
import levelup from './../images/levelup-icon.png';
import requestpoints from './../images/points-icon.png';
import requestsuccess from './../images/validation.png';

export default class Notifications extends React.Component {

  constructor(props) {
    super(props);
    this.state = {notification: this.props.notification};
  }

  componentDidUpdate(oldProps) {
    const newProps = this.props
    if(oldProps.notification !== newProps.notification) {
      this.setState({ user: newProps.notification });
    }
  }

  render() {   
    let image = '';
    let popupclass = '';
    if (this.state.notification){
      console.log(this.state.notification)
      if (this.state.notification.type === 'coins'){
        image = coinsGiven;
        popupclass = 'mb-1 alert alert-info fade show'
      } else if (this.state.notification.type === 'levelup') {
        image = levelup;
        popupclass = 'mb-1 alert alert-warning fade show';
      } else if (this.state.notification.type === 'requestPoints'){
        image = requestpoints;
        popupclass = 'mb-1 alert alert-secondary fade show';
      } else if (this.state.notification.type === 'requestSuccess') {
        image = requestsuccess;
        popupclass = 'mb-1 alert alert-success fade show';
      } else if (this.state.notification.type === 'volunteers') {
        image = requestpoints;
        popupclass = 'mb-1 alert alert-dark fade show';
      }
    }    

    return (  
      <div>
      {this.state.notification
      ? <Alert className={popupclass}>
      <div style={{fontWeight:'bold', fontSize: "18px"}}> 
        <img alt='Action done' src={image} style={{width:"30px", marginTop:"-10px", marginRight:'10px'}}/> 
        <span>{this.state.notification.title}</span>
      </div>
      {this.state.notification.description}
    </Alert>
      :<span></span>
    }    
    </div>
      
  )
}}