import React from "react";
import { Container, Button } from "shards-react";
import { Link } from "react-router-dom";

const Errors = () => (
  <Container fluid className="main-content-container px-4 pb-4">
    <div className="error">
      <div className="error__content">
        <h2>No access</h2>
        <h3>Special rights needed for this page!</h3>
        <p>Your account has no access to this page. Login with a proper account or contact your administrator.</p>
        <Button pill tag={Link} to="/">&larr; Login</Button>
      </div>
    </div>
  </Container>
);

export default Errors;
