import React from "react";
import { Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  ListGroup,
  ListGroupItem,
  FormCheckbox,
  FormInput,
  Form, 
  Button, 
  FormTextarea, 
  FormGroup,
  InputGroup,
  FormSelect } from "shards-react";
  import axios from 'axios';
import PageTitle from "../components/common/PageTitle";


export default class Editor extends React.Component {

  sendRequest() {

    let user=JSON.parse(localStorage.getItem("userDetails"));
    var editorValue = document.getElementById('editorValue').value;
    var titleValue = document.getElementById('titleValue').value;
    var numberVolunteerValue = document.getElementById('numberVolunteerValue').value;
    var numberPointsValue = document.getElementById('numberPointsValue').value;
    var categoryValue = document.getElementById('categoryValue').value;
    let categorytheme="dark";

                  axios({
                    method: 'post',
                    url: "http://localhost:8080/awards/request", 
                    data: {
                      title: titleValue,
                      body: editorValue,
                      userId:user.userId,
                      volunteersNeeded:numberVolunteerValue,
                      pointsNeeded:numberPointsValue,
                      category:categoryValue,
                      categoryTheme: categorytheme
                    }
                  }).then(res => {
                     alert("Your request is published Succesfully");
                     document.getElementById('editorValue').value = '';
                     document.getElementById('titleValue').value = '';
                     document.getElementById('numberVolunteerValue').value = '';
                     document.getElementById('numberPointsValue').value = '';
                    }).catch((error)=>{
                      alert(error);
                    })

    
  }
  

  render() {
    return (
      <Container fluid className="main-content-container px-4 pb-4">
    {/* Page Header */}
    <Row noGutters className="page-header py-4">
      <PageTitle sm="4" title="Post a new idea" subtitle="Your idea" className="text-sm-left" />
    </Row>

    <Row>
      {/* Editor */}
      <Col lg="9" md="12">
      <Card small className="mb-3">
        <CardBody>
          <Form className="add-new-post">
            
            <div style={{'fontSize':'18px', fontWeight:'300', marginBottom:'20px'}}>Please add your idea below:</div>
            {/* <ReactQuill id="editorValue" className="add-new-post__editor mb-1" /> */}
            <FormGroup>
              <FormInput id="titleValue" placeholder="Title" />
            </FormGroup>
            <FormTextarea id="editorValue" placeholder="Description" type='textarea' className="add-new-post__editor mb-1"></FormTextarea>
            <Button theme="accent" onClick={this.sendRequest.bind(this)}>Publish</Button>
          </Form>
            
        </CardBody>
      </Card>
      </Col>

      {/* Sidebar Widgets */}
      <Col lg="3" md="12">
      <Card small className="mb-3">
    <CardHeader className="border-bottom">
      <h6 className="m-0">Additional Options</h6>
    </CardHeader>

    <CardBody className="p-0">
      <ListGroup flush>
        <ListGroupItem className="p-3">
          <div className="d-flex mb-2">
            <i className="material-icons mr-1">flag</i>
            <strong className="mr-1">Maximum number of volunteers:</strong> 
            <div>
            <FormInput id="numberVolunteerValue" style={{width:'100px'}} type="number"></FormInput>
            </div>
          </div>
          <span className="d-flex mb-2">
            <i className="material-icons mr-1">attach_money</i>
            <strong className="mr-1">Maximum number of points: </strong>
            <div>
            <FormInput id="numberPointsValue" style={{width:'100px'}} type="number"></FormInput>
            </div>
          </span>
        </ListGroupItem>
      </ListGroup>
    </CardBody>
  </Card>
  <Card small className="mb-3">
    <CardHeader className="border-bottom">
      <h6 className="m-0">Volunteer Category</h6>
    </CardHeader>
    <CardBody className="p-0">
      <ListGroupItem className="d-flex px-3">
            <InputGroup className="ml-auto">
            <FormSelect
              size="sm"
              id="categoryValue"
              style={{ maxWidth: "230px" }}>
                  <option value="nocategory">Select a category</option>
                  <option value='social' key='social'>Social</option>
                  <option value='sports' key='sports'>Sports</option>
                  <option value='diversity' key='diversity'>Diversity</option>
                  <option value='environment' key='environment'>Environment</option>
                </FormSelect>
            </InputGroup>
          </ListGroupItem>
    </CardBody>
  </Card>
      </Col>
    </Row>
  </Container>
  
)};

}