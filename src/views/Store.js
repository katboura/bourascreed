import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody
} from "shards-react";
import {
  CardFooter,
  Badge,
  Button
} from "shards-react";
import PageTitle from "../components/common/PageTitle";
import user1 from './../images/avatars/1.jpg';
import user2 from './../images/avatars/2.jpg';
import user0 from './../images/avatars/0.jpg';
import one from './../images/store/one.jpg';
import two from './../images/store/two.jpg';
import three from './../images/store/three.jpg';
import four from './../images/store/four.jpg';
import five from './../images/store/five.jpg';
import six from './../images/store/six.jpg';
import seven from './../images/store/seven.jpg';
import eight from './../images/store/eight.jpg';
import nine from './../images/store/nine.jpg';

export default class UserAccountDetails extends React.Component {


  constructor(props) {
    super(props);

    this.state = {
      requests: ''
    };
  }



  getImageForUser(userName) {
    let image;
    if (userName === 'Katerina Boura') {
      image = user2;
    } else if (userName === 'Natasa Leonti') {
      image = user0;
    } else {
      image = user1;
    }
    return image;
  }


  render() {

    let userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.requests = [];

    //filter through requests to get the logged in users requests
    if (this.state.requests && this.state.requests.length > 0) {
      this.requests = this.state.requests.filter((function (request, id) {
        return request.user === userDetails.name;
      }));
    }

    if (this.state.requests && this.state.requests.length > 0) {
      this.othersRequests = this.state.requests.filter((function (request,
        id) {
        return request.user !== userDetails.name;
      }));
    }

    return (
      <Container fluid className="main-content-container px-4">
        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <PageTitle sm="4" title="Most wanted" subtitle="" className="text-sm-left" />
        </Row>

        {/* Third Row of Posts */}
        <Row>

          <Col lg="4" >
            <Card small className="card-post mb-4">
              <CardBody style={{ height: '200px' }} >
                <img
                  style={{ width: '100%' }}
                  src={one}
                  alt="amazon"
                  width="110"
                />
              </CardBody>
            </Card>
          </Col>

          <Col lg="4" >
            <Card small className="card-post mb-4">
              <CardBody style={{ height: '200px' }} >
                <img
                  style={{ width: '100%' }}
                  src={four}
                  alt="udemy"
                  width="110"
                />
              </CardBody>
            </Card>
          </Col>


          <Col lg="4" >
            <Card small className="card-post mb-4">
              <CardBody style={{ height: '200px' }} >
                <img
                  style={{ width: '100%' }}
                  src={eight}
                  alt="plaisio"
                  width="110"
                />
              </CardBody>
            </Card>
          </Col>

        </Row>

        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <PageTitle sm="4" title="Redeem your coins" subtitle="" className="text-sm-left" />
        </Row>

        {/* Third Row of Posts */}
        <Row>

          <Col lg="4" >
            <Card small className="card-post mb-4">
              <CardBody style={{ height: '200px' }} >
                <img
                  style={{ width: '100%' }}
                  src={one}
                  alt="codehub"
                  width="110"
                />
              </CardBody>
            </Card>
          </Col>

          <Col lg="4" >
            <Card small className="card-post mb-4">
              <CardBody style={{ height: '200px' }} >
                <img
                  style={{ width: '100%' }}
                  src={two}
                  alt="intersport"
                  width="110"
                />
              </CardBody>
            </Card>
          </Col>


          <Col lg="4" >
            <Card small className="card-post mb-4">
              <CardBody style={{ height: '200px' }} >
                <img
                  style={{ width: '100%' }}
                  src={three}
                  alt="sklavenitis"
                  width="110"
                />
              </CardBody>
            </Card>
          </Col>

        </Row>


        <Row>

          <Col lg="4" >
            <Card small className="card-post mb-4">
              <CardBody style={{ height: '200px' }} >
                <img
                  style={{ width: '100%' }}
                  src={four}
                  alt="amazon"
                  width="110"
                />
              </CardBody>
            </Card>
          </Col>

          <Col lg="4" >
            <Card small className="card-post mb-4">
              <CardBody style={{ height: '200px' }} >
                <img
                  style={{ width: '100%' }}
                  src={five}
                  alt="udemy"
                  width="110"
                />
              </CardBody>
            </Card>
          </Col>


          <Col lg="4" >
            <Card small className="card-post mb-4">
              <CardBody style={{ height: '200px' }} >
                <img
                  style={{ width: '100%' }}
                  src={six}
                  alt="plaisio"
                  width="110"
                />
              </CardBody>
            </Card>
          </Col>

        </Row>


        <Row>

          <Col lg="4" >
            <Card small className="card-post mb-4">
              <CardBody style={{ height: '200px' }} >
                <img
                  style={{ width: '100%' }}
                  src={seven}
                  alt="sb"
                  width="110"
                />
              </CardBody>
            </Card>
          </Col>

          <Col lg="4" >
            <Card small className="card-post mb-4">
              <CardBody style={{ height: '200px' }} >
                <img
                  style={{ width: '100%' }}
                  src={eight}
                  alt="kotsovolos"
                  width="110"
                />
              </CardBody>
            </Card>
          </Col>


          <Col lg="4" >
            <Card small className="card-post mb-4">
              <CardBody style={{ height: '200px' }} >
                <img
                  style={{ width: '100%' }}
                  src={nine}
                  alt="venetis"
                  width="110"
                />
              </CardBody>
            </Card>
          </Col>

        </Row>


      </Container>

      // <Container fluid className="main-content-container px-4">
      //   {/* Page Header */}
      //   <Row noGutters className="page-header py-4">
      //     <PageTitle sm="4" title="My requests" subtitle="Requests" className="text-sm-left" />
      //   </Row>

      //  {/* Users +1 Requests Table */}
      //  <Row>
      //   <Col>
      //     <Card small className="mb-4">
      //       <CardHeader className="border-bottom">
      //         <h6 className="m-0">+1 Requests</h6>
      //       </CardHeader>
      //       <CardBody className="p-0 pb-3">
      //       {this.requests.length > 0 ? 
      //         <table className="table mb-0">
      //           <thead className="bg-light">
      //             <tr>
      //               <th scope="col" className="border-0">
      //                 Request
      //               </th>
      //               <th scope="col" className="border-0">
      //                 Current Rating
      //               </th>
      //               <th scope="col" className="border-0">
      //                 Current Status
      //               </th>
      //             </tr>
      //           </thead>
      //           <tbody>
      //           {this.requests.map(function (request, id) {
      //             return <tr key={id}>
      //                     <td>
      //                     {request.description}
      //                     </td>
      //                     <td>
      //                     {request.coins}
      //                     </td> 
      //                     <td className={request.status === 'successful pass'?'successfulpass':''}>
      //                     {request.status}
      //                     </td>                 
      //                   </tr>
      //           })}
      //           </tbody>
      //         </table>
      //         : 
      //         <div className="p-3 list-group-item">No user requests available</div>}
      //       </CardBody>
      //     </Card>
      //   </Col>
      // </Row>
      // {/* End of Table */}
      // </Container>
    )
  }
};
