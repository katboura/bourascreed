import React from "react";
import { Container, Row, Col } from "shards-react";

import PageTitle from "../components/common/PageTitle";
import EditorIdeas from "../components/add-new-post/EditorIdeas";
import SidebarCategoriesProposal from "../components/add-new-post/SidebarCategoriesProposal";

const AddNewPost = () => (
  <Container fluid className="main-content-container px-4 pb-4">
    {/* Page Header */}
    <Row noGutters className="page-header py-4">
      <PageTitle sm="4" title="Post a new idea" subtitle="Your idea" className="text-sm-left" />
    </Row>

    <Row>
      {/* Editor */}
      <Col lg="9" md="12">
        <EditorIdeas />
      </Col>

      {/* Sidebar Widgets */}
      <Col lg="3" md="12">
        <SidebarCategoriesProposal />
      </Col>
    </Row>
  </Container>
);

export default AddNewPost;
