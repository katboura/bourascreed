import React from "react";
import { Container, Row, Col, Card, CardBody} from "shards-react";
import {
  CardFooter,
  Badge,
  Button
} from "shards-react";
import PageTitle from "../components/common/PageTitle";
import user1 from './../images/avatars/1.jpg';
import user2 from './../images/avatars/2.jpg';
import user0 from './../images/avatars/0.jpg';
import user3 from './../images/avatars/4.jpg';
import axios from 'axios';

export default class UserAccountDetails extends React.Component {


constructor(props) {
    super(props);

    this.state = {requests: ''};
  }

  componentDidMount() {
    axios.get('http://localhost:8080/awards/request/all').then(res => {
      console.log(res.data)
      this.setState({requests: res.data});
    }).catch(console.log) 
  }

  getImageForUser(userName){
    let image;
      if(userName === 'katerina'){
        image = user2;
      } else if (userName === 'natasa') {
        image = user0;
      }else if (userName === 'george') {
        image = user3;
      } else{
        image = user1;
      }
      return image;
  }

  getStatusForRequest(status){
    if (status === 1){
      return 'pending sponsor';
    }
    else if (status === 2) {
      return 'pending volunteer';
    } else {
      return 'success';
    }
  }

  volunteer(requestId){
   let userDetails = JSON.parse(localStorage.getItem('userDetails'));

   for (var i = 0; i < this.state.requests.length; i++) {
    let request = this.state.requests[i];
    if(request.requestId === requestId){
      let foundMatchingUser = false;
      if (request.usersVolunteered.length > 0 ) {
        for (var j = 0; j < request.usersVolunteered.length; j++){
          let userVolunteer = request.usersVolunteered[j];
          
          if (userVolunteer.username === userDetails.username){
            foundMatchingUser = true;
            document.getElementById("volunteer#"+requestId).disabled = true;
          }    
        }
        if (!foundMatchingUser) {
          request.usersVolunteered.push(userDetails);
          let data = this.state.requests;
          this.setState({requests:data});
          axios.post(`http://localhost:8080/awards/request/volunteer/`+userDetails.userId+'/'+requestId).then((res) => {
            }).catch(function(error){
              console.log(error)
            })
        }
      } else if (request.usersVolunteered.length === 0 ) {
        request.usersVolunteered.push(userDetails);
        let data = this.state.requests;
        this.setState({requests:data});
        axios.post(`http://localhost:8080/awards/request/volunteer/`+userDetails.userId+'/'+requestId).then((res) => {
            console.log(res)
            }).catch(function(error){
              console.log(error)
            })
      }
     }   
   }

  }

  render() {
  
    let userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.requests = [];

    //filter through requests to get the logged in users requests
    if (this.state.requests && this.state.requests.length>0){
      this.requests = this.state.requests.filter(( function (request, id) {
        return request.user.username === userDetails.username;
      }));
    }

    if (this.state.requests && this.state.requests.length>0){
      this.othersRequests = this.state.requests.filter(( function (request, id) {
        return request.user.username !== userDetails.username;
      }));
    }

  return (
  <Container fluid className="main-content-container px-4">
  {/* Page Header */}
  <Row noGutters className="page-header py-4">
      <PageTitle sm="4" title="Your Ideas" subtitle="Ideas" className="text-sm-left" />
    </Row>
  
    {/* Third Row of Posts */}
    <Row>
      {this.requests && this.requests.length>0 ? this.requests.map((post, idx) => (
        <Col lg="4" key={idx}>
          <Card small className="card-post mb-4">
            <CardBody style={{height: '300px'}} >
              <h5 className="card-title">{post.title}</h5>
              <p className="card-text text-muted">{post.body}</p>
              <Badge
                pill
                className={`card-post__category bg-${post.category_theme}`}
                >
                {post.category}
              </Badge>
              <div style={{paddingTop: '10px',paddingLeft: '5px',fontSize: '12px'}}>Status: <span style={{color:'brown'}}>{this.getStatusForRequest(post.status)}</span></div>
              <div style={{paddingTop: '7px',paddingLeft: '5px',fontSize: '12px'}}>Maximum points needed: <span style={{color:''}}>{post.maxPointsNeeded}</span></div>
              <div style={{paddingTop: '7px',paddingLeft: '5px',fontSize: '12px'}}>Points gathered: {post.points}</div>
              {post.usersVolunteered.length>0 ? <div style={{paddingTop: '7px',paddingLeft: '5px',fontSize: '12px', float: 'left', paddingRight:'5px'}}>Volunteers: </div>: null}

              {/* Volunteers section */}
              {post.usersVolunteered.length>0 ? post.usersVolunteered.map((item, id) =>(
                <img
                key={id}
                alt={item.username}
                title={item.firstName+' already volunteered'}
                style={{float:'left'}}
                className="card-post__author-avatar card-post__author-avatar--small"
                src={this.getImageForUser(item.username)}
                />
                ))              
              :
              null
              }
              
            </CardBody>
            <CardFooter className="border-top d-flex">
              <div className="card-post__author d-flex">
                <img
                alt={post.user.username}
                href="#"
                className="card-post__author-avatar card-post__author-avatar--small"
                src={this.getImageForUser(post.user.username)}
                />
               <span style={{paddingTop: '7px',paddingLeft: '5px',fontSize: '12px'}}>Proposed by you</span>
                <div className="d-flex flex-column justify-content-center ml-3">
                  <span className="card-post__author-name">
                    {post.author}
                  </span>
                  <small className="text-muted">{post.date}</small>
                </div>
              </div>
            </CardFooter>
          </Card>
        </Col>
      )
      ) : <div>You haven't posted any ideas yet.</div> }
    </Row>

  {/* Page Header */}
  <Row noGutters className="page-header py-4">
      <PageTitle sm="4" title="Other People's Ideas" subtitle="" className="text-sm-left" />
    </Row>
  
    {/* Third Row of Posts */}
    <Row>
      {this.othersRequests && this.othersRequests.length>0 ? this.othersRequests.map((post, idx) => (
        <Col lg="4" key={idx}>
          <Card small className="card-post mb-4">
            <CardBody style={{height: '300px'}}>
              <h5 className="card-title">{post.title}</h5>
              <p className="card-text text-muted">{post.body}</p>
              <Badge
                pill
                className={`card-post__category bg-${post.category_theme}`}
                >
                {post.category}
              </Badge>
              <div style={{paddingLeft: '5px',fontSize: '12px', paddingTop:"10px"}}>Status: <span style={{color:'brown'}}>{this.getStatusForRequest(post.status)}</span></div>
              <div style={{paddingTop: '7px',paddingLeft: '5px',fontSize: '12px'}}>Points gathered: {post.points}</div>
              {post.usersVolunteered.length>0 ? <div style={{paddingTop: '7px',paddingLeft: '5px',fontSize: '12px', float: 'left', paddingRight:'5px'}}>Volunteers: </div>: null}

              {/* Volunteers section */}
              {post.usersVolunteered.length>0 ? post.usersVolunteered.map((item, id) =>(
                <img
                key={id}
                alt={item.username}
                title={item.firstName+' already volunteered'}
                style={{float:'left'}}
                className="card-post__author-avatar card-post__author-avatar--small"
                src={this.getImageForUser(item.username)}
                />
                ))              
              :
              null
              }

            </CardBody>
            <CardFooter className="border-top d-flex">
              <div className="card-post__author d-flex">   
                <img
                alt={post.user.username}
                href="#"
                className="card-post__author-avatar card-post__author-avatar--small"
                src={this.getImageForUser(post.user.username)}
                />
               <span style={{paddingTop: '7px',paddingLeft: '5px',fontSize: '12px'}}>Proposed by {post.user.firstName +' '+post.user.lastName}</span>
                <div className="d-flex flex-column justify-content-center ml-3">
                  <span className="card-post__author-name">
                    {post.author}
                  </span>
                  <small className="text-muted">{post.date}</small>
                </div>
              </div>
              {post.status === 2 
              ? <div className="my-auto ml-auto">
                  <Button size="sm" theme="white" id={'volunteer#'+post.requestId} onClick={() => this.volunteer(post.requestId)}>
                    <i className="far fa-thumbs-up mr-1" /> Volunteer
                  </Button>
                </div>
              : null}
              
            </CardFooter>
          </Card>
        </Col>
      )
      ) : <div style={{paddingLeft:"20px"}}>No ideas to for volunteer yet.</div> }
    </Row>
</Container>
)
}
};
