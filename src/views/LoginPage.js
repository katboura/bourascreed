import React from "react";
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
} from "shards-react";
import { Button, Form, Row, Col, FormInput } from "shards-react";
import axios from 'axios';
export default class LoginPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = ({ validationErrorShow: false });
  }

  getUsersInfo(user){
    if(user.username==='admin'){
        localStorage.setItem('role', 'admin');
        window.location = '/blog-overview'
    }else{
      axios.get('http://localhost:8080/awards/user/'+user.userId).then(res => {
         let userDetails = res.data;
         console.log(userDetails)
         localStorage.setItem('role', userDetails.role.roleTitle);
         localStorage.setItem('userDetails', JSON.stringify(userDetails));
         if(localStorage.getItem('mode') === 'plain') {
           window.location = '/feed'
         } else {
           window.location = '/blog-overview'
         }
       }).catch(console.log)
    }

     
  }

  loginAction(){
    
    let usernameValue = document.getElementById('username').value;
    let passwordValue = document.getElementById('password').value;
    
    console.log("prin to axisot");
    axios({
      method: 'post',
      url: "http://localhost:8080/awards/login", 
      data: {
        username: usernameValue,
        password: passwordValue
      }
    }).then(res => {
       console.log("edww ta alithina");
       console.log(res.data);
       
       let response=res.data;
       let user=response.user;
       
       if(user){
         localStorage.setItem('user',user.username);
         this.getUsersInfo(user);
         this.setState({ validationErrorShow: false });
       }else{
        this.setState({ validationErrorShow: true });
       }
      }).catch(console.log)

  }
  
  render() {
      return (
          <Card small className="mb-4">
            <CardHeader className="border-bottom">
              <h6 className="m-0">Login</h6>
            </CardHeader>

            <ListGroup flush>
              <ListGroupItem className="px-3">
              <Form>
            <Row form>
              {/* Name */}
              <Col md="10" className="form-group">
              <label htmlFor="username">Username</label>
                <FormInput
                  id="username"
                  placeholder="Username"
                />
              </Col>
            </Row>
            <Row form>

              <Col md="10" className="form-group">
              <label htmlFor="password">Password</label>
                <FormInput
                  id="password"
                  type="password"
                  placeholder="Password"
                />
              </Col>
              { this.state.validationErrorShow ?
              <div className="invalid-login">Wrong credentials! Please login again!</div>
              : null }
            </Row>
            
            <Button theme="accent" onClick={this.loginAction.bind(this)}>Login</Button>
          </Form>
          </ListGroupItem>
        </ListGroup>
      </Card>
    );
  }
}
