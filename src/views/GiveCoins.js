import React from "react";
import { Container, Row, Col, Card, CardHeader, CardBody, Button, FormSelect, ListGroupItem, InputGroup, FormInput, InputGroupAddon } from "shards-react";
import user1 from './../images/avatars/1.jpg';
import user2 from './../images/avatars/2.jpg';
import user0 from './../images/avatars/0.jpg';
import user3 from './../images/avatars/4.jpg';
import UserCard from './UserCard';
import Popup from "reactjs-popup";
import PageTitle from "../components/common/PageTitle";
import axios from 'axios';

export default class UserAccountDetails extends React.Component {


constructor(props) {
    super(props);

    this.state = {users: ''};
  }

  componentDidMount() {

    axios.get('http://localhost:8080/awards/user/all').then(res => {
      this.setState({users: res.data});
    }).catch(console.log)

      this.setState({categories:["Problem solving", "Innovation", "Leadership", "Vision", "Customer Service"]});
    
    }

    getImageForUser(userName){
      let image;
        if(userName === 'katerina'){
          image = user2;
        } else if (userName === 'natasa') {
          image = user0;
        }else if (userName === 'george') {
          image = user3;
        } else{
          image = user1;
        }
        return image;
    }

  vote(userId, userName){
    let userDetails = JSON.parse(localStorage.getItem('userDetails'));

    let voteCoins = document.getElementById('vote#'+userId).value;
    let categoriesValue = document.getElementById('categoriesValue#'+userId).value;
   
    console.log(voteCoins)
    console.log(categoriesValue)

    let data = this.state.users;
    for (var i = 0; i < data.length; i++) {
      if(data[i].userId === userId) {
        if (voteCoins){
          data[i].status = parseInt(data[i].status) +  parseInt(voteCoins);
          this.setState({users: data});
        }        
      }
    }

    //call service to save the vote
    if(categoriesValue !== 'nocategory' && voteCoins){
      alert("You just gave "+ voteCoins+ " coins to user "+ userName+ " in category: "+ categoriesValue);
      
      axios.post(`http://localhost:8080/awards/coins/send/`+userDetails.userId+'/'+userId+'?coins='+voteCoins+'&coinCategoryName='+categoriesValue).then((res) => {
        document.getElementById('categoriesValue#'+userId).value = "nocategory";
        document.getElementById('vote#'+userId).value = "";
        axios.get('http://localhost:8080/awards/user/'+userDetails.userId).then(res => {
          let details = res.data;
          localStorage.setItem('userDetails', JSON.stringify(details));
          this.setState({user: details});
        }).catch(console.log)
      }).catch(function(error){
        console.log(error)
      })
    } else {
      alert("Please select a coin category and number of coins before trying to add coins to a user");
    }
    
  }

  render() {

    let userDetails = JSON.parse(localStorage.getItem('userDetails'));
    let username = userDetails.username;

    if (this.state.categories){
      this.categories = this.state.categories.map((item) =>
      <option value={item} key={item}>{item}</option>
      );
    }  
  
    this.users = [];
    if (this.state.users && this.state.users.length>0){
      this.users = this.state.users.map((user, id) =>
      user.role && user.role.roleTitle === 'normal' && username && user.username !== username
      ? <tr key={id}>
        <td>
          <Popup
            trigger={<div style={{cursor:'pointer'}}> {user.firstName +' '+user.lastName} </div>}
            modal
            closeOnDocumentClick
          >
            <UserCard userSelectedFromParent = {user}/>
          </Popup>       
        </td>
        <td>
        <img
          key={id}
          alt={user.name}
          style={{float:'left'}}
          className="card-post__author-avatar card-post__author-avatar--small"
          src={this.getImageForUser(user.username)}
          />
        </td>
        <td>
        {user.currentBadge.badgeName}
        </td> 
        <td>
        {user.jobTitle.description}
        </td>
        <td>
          <ListGroupItem className="d-flex px-3">
            <InputGroup className="ml-auto">
            <FormSelect
              size="sm"
              id={"categoriesValue#"+user.userId}
              style={{ maxWidth: "230px" }}
              onChange={this.handleOnChange}>
                  <option value="nocategory">Select a coin category</option>
                  {this.categories}
                </FormSelect>
            </InputGroup>
          </ListGroupItem>
        </td>
        <td>
          <ListGroupItem className="d-flex px-3">
            <InputGroup className="ml-auto">
              <FormInput placeholder="Vote" type="number" id={'vote#'+user.userId} />
              <InputGroupAddon type="append">
                <Button theme="white" className="px-2" onClick={() => this.vote(user.userId, user.firstName+' '+user.lastName)}>
                  <i className="material-icons">add</i>
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </ListGroupItem>
        </td>                         
      </tr>
      : null
      );
    } 


  return (

  <Container fluid className="main-content-container px-4">
    {/* Page Header */}
    <Row noGutters className="page-header py-4">
      <PageTitle sm="4" title="Vote for users" subtitle="Users" className="text-sm-left" />
    </Row>

   {/* Users +1 Users Table */}
   <Row>
    <Col>
      <Card small className="mb-4">
        <CardHeader className="border-bottom">
          <h6 className="m-0">Give away coins to users</h6>
        </CardHeader>
        <CardBody className="p-0 pb-3">
        {this.users.length > 0 ? 
          <table className="table mb-0">
            <thead className="bg-light">
              <tr>
                <th scope="col" className="border-0">
                  User
                </th>
                <th scope="col" className="border-0">
                  
                </th>
                <th scope="col" className="border-0">
                  Badge of user
                </th>
                <th scope="col" className="border-0">
                  Position
                </th>
                <th scope="col" className="border-0">
                  Category
                </th>
                <th scope="col" className="border-0">
                </th>
              </tr>
            </thead>
            <tbody>
              {this.users}
            </tbody>
          </table>
          : 
          <div className="p-3 list-group-item">No user users available</div>}
        </CardBody>
      </Card>
    </Col>
  </Row>
  {/* End of Table */}
  </Container>
)
}
};
