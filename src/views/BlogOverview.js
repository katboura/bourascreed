import React from "react";
import { Container, Row, Col } from "shards-react";

import PageTitle from "./../components/common/PageTitle";
import SmallStats from "./../components/common/SmallStats";
import UsersOverview from "./../components/blog/UsersOverview";
import BadgesOverview from "./../components/blog/BadgesOverview";
import UsersByDevice from "./../components/blog/UsersByDevice";
import Discussions from "./../components/blog/Discussions";
import TopReferrals from "./../components/common/TopReferrals";
import UserBadgeArea from "./../components/common/UserBadgeArea";
import Notifications from './Notifications';
import axios from 'axios';

export default class BlogOverview extends React.Component {
  timeout= 0;
  constructor(props) {
    super(props);

    this.state = {user: '',
                  notifications: [],
                  mode:''};
      }

  componentDidMount() {

    let userDetails = JSON.parse(localStorage.getItem('userDetails'));
    if(userDetails){
      this.setState({user: userDetails});

    //the first time we need it to be called right away
    axios.get(`http://localhost:8080/awards/notifications/new/`+userDetails.userId).then(res => {
        this.setState({ notifications: res.data})
        if(res.data.length>0) {
          localStorage.setItem('newNotification', res.data[res.data.length-1].id)
        } else {
          localStorage.setItem('newNotification', 0)
        }
    })
  
  if (userDetails.role.roleTitle === 'normal'){
    this.timeout = setInterval(() => {
      let userDetails = JSON.parse(localStorage.getItem('userDetails'));
      let previousNewNotifications = localStorage.getItem('newNotification')

      axios.get(`http://localhost:8080/awards/notifications/new/`+userDetails.userId).then(res => {
          this.setState({ notifications: res.data})
          if(res.data.length>0) {
            localStorage.setItem('newNotification', res.data[res.data.length-1].id)
            if(parseInt(previousNewNotifications) !== res.data[res.data.length-1].id){
              //get user information again
              axios.get('http://localhost:8080/awards/user/'+userDetails.userId).then(res => {
                let details = res.data;
                localStorage.setItem('userDetails', JSON.stringify(details));
                this.setState({user: details});
              }).catch(console.log)
            }
          } else {
            localStorage.setItem('newNotification', 0)
          }
      })
    }, 3000);
  }
    }
    

  fetch('http://www.mocky.io/v2/5d63dee13200007700ba1e54')
  .then(res => res.json())
  .then((data) => {        
    console.log(data)
  })
  .catch(console.log)

    if (!localStorage.getItem('mode')){
      fetch('http://www.mocky.io/v2/5d5bfacb3200005100628c6b')
      .then(res => res.json())
      .then((data) => {
        localStorage.setItem('mode', data.mode);
      })
      .catch(console.log)
    }
  }

  componentWillUnmount(){
    clearInterval(this.timeout)
  }

  
  onExit(){
    let userDetails = JSON.parse(localStorage.getItem('userDetails'));
    
    //call service here
    let body = {};
    body.isNotificationRead = true;
    body.userId = userDetails.userId;
    axios.post(`http://localhost:8080/awards/notifications/setRead/`+userDetails.userId, body).then(() => {
    this.setState({notifications: null});
    localStorage.setItem('newNotification', 0)
  }).catch(function(error){
    console.log(error)
  })
  }


   
  render() {
    var smallStats =  [
          {
            label: "+1 Requests",
            value: "2,390",
            percentage: "4.7%",
            increase: true,
            chartLabels: [null, null, null, null, null, null, null],
            attrs: { md: "6", sm: "6" },
            datasets: [
              {
                label: "Today",
                fill: "start",
                borderWidth: 1.5,
                backgroundColor: "rgba(0, 184, 216, 0.1)",
                borderColor: "rgb(0, 184, 216)",
                data: [1, 2, 1, 3, 5, 4, 7]
              }
            ]
          },
          {
            label: "Badge acquire",
            value: "182",
            percentage: "12.4",
            increase: true,
            chartLabels: [null, null, null, null, null, null, null],
            attrs: { md: "6", sm: "6" },
            datasets: [
              {
                label: "Today",
                fill: "start",
                borderWidth: 1.5,
                backgroundColor: "rgba(23,198,113,0.1)",
                borderColor: "rgb(23,198,113)",
                data: [1, 2, 3, 3, 3, 4, 4]
              }
            ]
          },
          {
            label: "Coins given",
            value: "8,147",
            percentage: "3.8%",
            increase: false,
            decrease: true,
            chartLabels: [null, null, null, null, null, null, null],
            attrs: { md: "4", sm: "6" },
            datasets: [
              {
                label: "Today",
                fill: "start",
                borderWidth: 1.5,
                backgroundColor: "rgba(255,180,0,0.1)",
                borderColor: "rgb(255,180,0)",
                data: [2, 3, 3, 3, 4, 3, 3]
              }
            ]
          },
          {
            label: "New Users",
            value: "29",
            percentage: "2.71%",
            increase: false,
            decrease: true,
            chartLabels: [null, null, null, null, null, null, null],
            attrs: { md: "4", sm: "6" },
            datasets: [
              {
                label: "Today",
                fill: "start",
                borderWidth: 1.5,
                backgroundColor: "rgba(255,65,105,0.1)",
                borderColor: "rgb(255,65,105)",
                data: [1, 7, 1, 3, 1, 4, 8]
              }
            ]
          }
        ]

      this.notifications = null;
        if (this.state.notifications && this.state.notifications.length > 0){
          this.notifications = this.state.notifications.map((notification, id) =>
            <Notifications key={id} notification ={notification} notificationId ={notification.id}/> 
          );
        } else {
          this.notifications = null;
        }
      return (
    <div>
    {this.notifications
      ? <div
      onClick={() => this.onExit()}  
      style={{float:'right', position:'relative', bottom: '0px', top:'5px', fontFamily: 'inherit', fontWeight: 'lighter', fontSize: '15px', cursor: 'pointer',zIndex: '100000',color: 'white', right:'5px'}}>
      Hide all
    </div>
      : null
      }
   
    {this.notifications}
    
    <Container fluid className="main-content-container px-4">
      {/* Page Header */}
      <Row noGutters className="page-header py-4">
        {localStorage.getItem('user') === 'admin'
        ? <PageTitle title="General information" subtitle="Dashboard" className="text-sm-left mb-3" />
        :<PageTitle title={"Welcome back "+this.state.user.firstName+" "+this.state.user.lastName+"!"} subtitle="Dashboard" className="text-sm-left mb-3" 
        />
        }
      </Row>

      {/* Small Stats Blocks */}
        <br></br>
        <div style={{fontSize:"25px", marginLeft:"15px"}}>
           Your Statistics
        </div>
     
      <Row>
        {localStorage.getItem('user') === 'admin' ?
          smallStats.map((stats, idx) => (
          <Col className="col-lg mb-4" key={idx} {...stats.attrs}>
            <SmallStats
              id={`small-stats-${idx}`}
              variation="1"
              chartData={stats.datasets}
              chartLabels={stats.chartLabels}
              label={stats.label}
              value={stats.value}
              percentage={stats.percentage}
              increase={stats.increase}
              decrease={stats.decrease}
            />
          </Col>
        ))
        : <UserBadgeArea user={this.state.user}/>
        }
      </Row>

      <br/>
      <br/>
        <div style={{fontSize:"25px", marginLeft:"15px"}}>
           Other Users Statistics
        </div>
        <br/>
      <Row>
        {/* Users Overview */}
        <Col lg="8" md="12" sm="12" className="mb-4">
       {localStorage.getItem('user') === 'admin'
        ?<UsersOverview />
        :<BadgesOverview/>}
        </Col>

        {/* Users by Device */}
        <Col lg="4" md="12" sm="12" className="mb-4">
          <UsersByDevice />
        </Col>

        {/* Discussions */}
        <Col lg="5" md="12" sm="12" className="mb-4">
          <Discussions />
        </Col>

        {/* Top Referrals */}
        <Col lg="7" md="12" sm="12" className="mb-4">
          <TopReferrals />
        </Col>
      </Row>
  </Container>
  </div>
    )
  }
};