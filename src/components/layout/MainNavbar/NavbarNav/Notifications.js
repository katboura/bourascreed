import React from "react";
import coinsGiven from '../../../../images/coinsToGive.png';
import coinstogive from '../../../../images/coinsICollect.png';
import sponsorImg from '../../../../images/points-icon.png';

export default class Notifications extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false,
      user:''
    };

  }

  componentDidMount() {

    let userDetails = JSON.parse(localStorage.getItem('userDetails'));
      this.setState({user: userDetails});
       
    this.timeout = setInterval(() => {
      let userDetails = JSON.parse(localStorage.getItem('userDetails'));
      this.setState({user: userDetails});
    }, 3000);
  }

  componentWillUnmount(){
    clearInterval(this.timeout)
  }

  render() {
    return (
    <div>
      {this.state.user ?
      <ul className="border-left flex-row navbar-nav">
         {this.state.user.role.roleTitle === 'normal' ?
         <li className="border-right dropdown notifications nav-item">
          <span className="nav-link-icon text-center nav-link">
            <div className="nav-link-icon__wrapper">
              <img alt='Coins from others' src={coinsGiven} style={{width:'25px', paddingTop:'10px'}}/>
             <span className="badge badge-danger badge-pill">{this.state.user.coinsReceivedFromOtherUsers}</span>
            </div>
          </span>
        </li>
        : null}
        {this.state.user.role.roleTitle === 'normal' ? 
        <li className="border-right dropdown notifications nav-item">
          <span className="nav-link-icon text-center nav-link">
            <div className="nav-link-icon__wrapper">
              <img alt='Coins to give away' src={coinstogive} style={{width:'25px', paddingTop:'8px'}}/>
              <span className="badge badge-danger badge-pill">{this.state.user.coinsBalance}</span>
            </div>
          </span>
        </li>
        : null}
        {this.state.user.role.roleTitle === 'sponsor' ? 
        <li className="border-right dropdown notifications nav-item">
          <span className="nav-link-icon text-center nav-link">
            <div className="nav-link-icon__wrapper">
              <img alt='Coins for requests' src={sponsorImg} style={{width:'25px', paddingTop:'8px'}}/>
              <span className="badge badge-danger badge-pill">{this.state.user.coinsForUpvotingRequests}</span>
            </div>
          </span>
        </li>
        : null}
      </ul>
      : null}
    </div>
    );
  }
}
