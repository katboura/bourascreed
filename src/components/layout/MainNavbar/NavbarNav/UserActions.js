import React from "react";
import { Link } from "react-router-dom";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Collapse,
  NavItem,
  NavLink
} from "shards-react";
import user1image from './../../../../images/avatars/0.jpg';
import user2image from './../../../../images/avatars/1.jpg';
import user3image from './../../../../images/avatars/2.jpg';
import user4image from './../../../../images/avatars/3.jpg';
import user5image from './../../../../images/avatars/4.jpg';


export default class UserActions extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false
    };

    this.toggleUserActions = this.toggleUserActions.bind(this);
  }

  toggleUserActions() {
    this.setState({
      visible: !this.state.visible
    });
  }

  logoutAction(){
    localStorage.removeItem('user');
    localStorage.removeItem('role');
    localStorage.removeItem('userDetails');
    localStorage.removeItem('newNotification');
  }

  componentDidMount() {
    if (localStorage.getItem('user') === 'admin') {
      this.setState({
        user: "Administrator",
        image:user4image
      })
    } else if (localStorage.getItem('user') === 'katerina') {
      this.setState({
        user: "Katerina Boura",
        image:user3image
      })
    } else if (localStorage.getItem('user') === 'natasa') {
      this.setState({
        user: "Natasa leonti",
        image:user1image
      })
    } else if (localStorage.getItem('user') === 'thodoris') {
      this.setState({
        user: "Theo Bouras",
        image:user2image
      })
    } else if (localStorage.getItem('user') === 'george') {
      this.setState({
        user: "George Kastanas",
        image:user5image
      })
    }
  }

  render() {
    return (
      <NavItem tag={Dropdown} caret toggle={this.toggleUserActions}>
        <DropdownToggle tag={NavLink} className="text-nowrap px-3">
          <img
            className="user-avatar rounded-circle mr-2"
            src={this.state.image}
            alt="User Avatar"
          />{" "}
          <span className="d-none d-md-inline-block">{this.state.user}</span>
        </DropdownToggle>
        <Collapse tag={DropdownMenu} right small open={this.state.visible}>
          {/* <DropdownItem tag={Link} to="user-profile">
            <i className="material-icons">&#xE7FD;</i> Profile
          </DropdownItem>
          <DropdownItem tag={Link} to="edit-user-profile">
            <i className="material-icons">&#xE8B8;</i> Edit Profile
          </DropdownItem>
          <DropdownItem tag={Link} to="file-manager-list">
            <i className="material-icons">&#xE2C7;</i> Files
          </DropdownItem>
          <DropdownItem tag={Link} to="transaction-history">
            <i className="material-icons">&#xE896;</i> Transactions
          </DropdownItem>
          <DropdownItem divider /> */}
          <DropdownItem tag={Link} to="/" onClick={this.logoutAction.bind(this)} className="text-danger">
            <i className="material-icons text-danger">&#xE879;</i> Logout
          </DropdownItem>
        </Collapse>
      </NavItem>
    );
  }
}
