import React from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardHeader,
  CardBody,
  ListGroup,
  ListGroupItem,
  FormCheckbox
} from "shards-react";

const SidebarCategories = () => (
  <Card small className="mb-3">
    <CardHeader className="border-bottom">
      <h6 className="m-0">Volunteer Category</h6>
    </CardHeader>
    <CardBody className="p-0">
      <ListGroup flush>
        <ListGroupItem className="px-3 pb-2">
          <FormCheckbox className="mb-1" value="general" defaultChecked>
            General
          </FormCheckbox>
          <FormCheckbox className="mb-1" value="social">
            Social
          </FormCheckbox>
          <FormCheckbox className="mb-1" value="sports">
            Sports
          </FormCheckbox>
          <FormCheckbox className="mb-1" value="diversity">
            Diversity
          </FormCheckbox>
          <FormCheckbox className="mb-1" value="environment">
            Environment
          </FormCheckbox>
        </ListGroupItem>
      </ListGroup>
    </CardBody>
  </Card>
);

SidebarCategories.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string
};

SidebarCategories.defaultProps = {
  title: "Categories"
};

export default SidebarCategories;
