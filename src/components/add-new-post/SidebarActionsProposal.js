/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardHeader,
  CardBody,
  ListGroup,
  ListGroupItem,
  FormInput
} from "shards-react";

const SidebarActions = ({ title }) => (
  <Card small className="mb-3">
    <CardHeader className="border-bottom">
      <h6 className="m-0">{title}</h6>
    </CardHeader>

    <CardBody className="p-0">
      <ListGroup flush>
        <ListGroupItem className="p-3">
          <div className="d-flex mb-2">
            <i className="material-icons mr-1">flag</i>
            <strong className="mr-1">Maximum number of volunteers:</strong> 
            <div>
            <FormInput style={{width:'100px'}} type="number"></FormInput>
            </div>
          </div>
          <span className="d-flex mb-2">
            <i className="material-icons mr-1">attach_money</i>
            <strong className="mr-1">Maximum number of points: </strong>
            <div>
            <FormInput style={{width:'100px'}} type="number"></FormInput>
            </div>
          </span>
        </ListGroupItem>
      </ListGroup>
    </CardBody>
  </Card>
);

SidebarActions.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string
};

SidebarActions.defaultProps = {
  title: "Actions"
};

export default SidebarActions;
