import React from "react";
// import ReactQuill from "react-quill";
import { Card, CardBody, Form, Button, FormTextarea } from "shards-react";

import "react-quill/dist/quill.snow.css";
import "../../assets/quill.css";

export default class Editor extends React.Component {

  sendRequest() {
    var editorValue = document.getElementById('editorValue').value;
    console.log('this is the request: ', editorValue);
    document.getElementById('editorValue').value = '';
  }

  render() {
    return (
  <Card small className="mb-3">
    <CardBody>
      <Form className="add-new-post">
        <div style={{'fontSize':'18px', fontWeight:'300', marginBottom:'20px'}}>Please add below the details of your request:</div>
        {/* <ReactQuill id="editorValue" className="add-new-post__editor mb-1" /> */}
        <FormTextarea id="editorValue" type='textarea' className="add-new-post__editor mb-1"></FormTextarea>
        <Button theme="accent" onClick={this.sendRequest.bind(this)}>Publish</Button>
      </Form>
        
    </CardBody>
  </Card>
)};

}
