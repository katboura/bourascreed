import React from "react";
// import ReactQuill from "react-quill";
import { Card, CardBody, Form, Button, FormTextarea, FormInput, FormGroup } from "shards-react";

import "react-quill/dist/quill.snow.css";
import "../../assets/quill.css";

export default class Editor extends React.Component {

  sendRequest() {
    var editorValue = document.getElementById('editorValue').value;
    var titleValue = document.getElementById('titleValue').value;
    console.log('this is the request: ', titleValue, editorValue);
    document.getElementById('editorValue').value = '';
    document.getElementById('titleValue').value = '';
  }

  render() {
    return (
  <Card small className="mb-3">
    <CardBody>
      <Form className="add-new-post">
        
        <div style={{'fontSize':'18px', fontWeight:'300', marginBottom:'20px'}}>Please add your idea below:</div>
        {/* <ReactQuill id="editorValue" className="add-new-post__editor mb-1" /> */}
        <FormGroup>
          <FormInput id="titleValue" placeholder="Title" />
        </FormGroup>
        <FormTextarea id="editorValue" placeholder="Description" type='textarea' className="add-new-post__editor mb-1"></FormTextarea>
        <Button theme="accent" onClick={this.sendRequest.bind(this)}>Publish</Button>
      </Form>
        
    </CardBody>
  </Card>
)};

}
