import React from "react";
import {
  Card,
  CardHeader
} from "shards-react";
import user1 from './../../images/avatars/1.jpg';
import user2 from './../../images/avatars/2.jpg';
import user0 from './../../images/avatars/0.jpg';
import user3 from './../../images/avatars/4.jpg';
export default class UserDetailsCard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {user: this.props.user};
  }
  
  componentDidUpdate(oldProps) {
    const newProps = this.props
    if(oldProps.user !== newProps.user) {
      this.setState({ user: newProps.user });
    }
  }

  getImageForUser(userName){
    let image;
      if(userName === 'katerina'){
        image = user2;
      } else if (userName === 'natasa') {
        image = user0;
      }else if (userName === 'george') {
        image = user3;
      } else{
        image = user1;
      }
      return image;
  }
  
  render() {
      return (
        <div>
        { this.state.user ?    
          <Card small className="mb-4 pt-3">
          <CardHeader className="border-bottom text-center">
            <div className="mb-3 mx-auto">
              <img
                className="rounded-circle"
                src={this.getImageForUser(this.state.user.username)}
                alt={this.state.user.firstName}
                width="110"
              />
            </div>
            <h4 className="mb-0">{this.state.user.firstName +' '+ this.state.user.lastName}</h4>
            <span className="text-muted d-block mb-2">{this.state.user.email}</span>
            {this.state.user.jobTitle?<span className="text-muted d-block mb-2">{this.state.user.jobTitle.description}</span>:null}
          </CardHeader>
        </Card>
         : null }
     </div>
    );
  }
}