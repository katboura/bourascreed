import React from "react";
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Progress
} from "shards-react";

export default class UserDetails extends React.Component {

  constructor(props) {
    super(props);
    this.state = {user: this.props.user};
  }
  
  componentDidUpdate(oldProps) {
    const newProps = this.props
    if(oldProps.user !== newProps.user) {
      this.setState({ user: newProps.user });
    }
  }
  
  render() {
      return (
        <div>
        { this.state.user.value !== "nouser" ?    
          <Card small className="mb-4 pt-3">
          <CardHeader className="border-bottom text-center">
            <div className="mb-3 mx-auto">
              <img
                className="rounded-circle"
                src={this.state.user.avatar}
                alt={this.state.user.name}
                width="110"
              />
            </div>
            <h4 className="mb-0">{this.state.user.name}</h4>
            <span className="text-muted d-block mb-2">{this.state.user.jobTitle}</span>
          </CardHeader>
          <ListGroup flush>
            <ListGroupItem className="px-4">
              <div className="progress-wrapper">
                <strong className="text-muted d-block mb-2">
                  Total coins from others
                </strong>
                <Progress
                  className="progress-sm"
                  value={this.state.user.totalCoinsFromOthers/1000}
                >
                  <span className="progress-value">
                    {this.state.user.totalCoinsFromOthers}
                  </span>
                </Progress>
                
              </div>
            </ListGroupItem>
            <ListGroupItem className="px-4">
              <div className="progress-wrapper">
                <strong className="text-muted d-block mb-2">
                  Total coins to give
                </strong>
                <Progress
                  theme="success"
                  className="progress-sm"
                  value={this.state.user.totalCoinsToGive/1000}
                >
                  <span className="progress-value">
                    {this.state.user.totalCoinsToGive}
                  </span>
                </Progress>
                
              </div>
            </ListGroupItem>
            <ListGroupItem className="p-4">
              <strong className="text-muted d-block mb-2">
                {this.state.user.metaTitle}
              </strong>
              <span>{this.state.user.metaValue}</span>
            </ListGroupItem>
          </ListGroup>
        </Card>
         : null }
     </div>
    );
  }
}