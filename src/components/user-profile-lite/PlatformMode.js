import React from "react";
import {
  Card,
  CardHeader,
  ListGroup,
  FormRadio,
  ListGroupItem,
  Form,
  Button,
  Row,
  Col
} from "shards-react";

export default class PlatformMode extends React.Component {

  constructor(props) {
    super(props);
    this.state = {mode: ""};
  }

  componentDidMount() {
    this.setState({ mode: localStorage.getItem('mode') });
  }

  onModeChange(){
    if (this.state.mode === 'plain') {
      this.setState({ mode: "plus" });
    } else {
      this.setState({ mode: "plain" });
    }  

  }

  onModeSubmit(){
    console.log('button pressed mode ',this.state.mode);
    alert('Platform mode changed.')  
    localStorage.setItem('mode', this.state.mode);
    
    //we can refresh after service call and fetch the new values
    // window.location.reload();
  }


  render() {
      return (
        <div>
          <Card small className="mb-4">
            <CardHeader className="border-bottom">
              <h6 className="m-0">Platform Mode</h6>
            </CardHeader>
            <Form>
              <ListGroup flush>
                <ListGroupItem className="p-0 px-3 pt-3">
                  <Row>
                  <Col sm="12" md="4" className="mb-3">
                    <fieldset>
                      <FormRadio id="plusValue" checked={this.state.mode === "plus"} onClick={this.onModeChange.bind(this)}>Great & Plus one Gamification Platform</FormRadio>
                      <FormRadio id="plainValue" checked={this.state.mode === "plain"} onClick={this.onModeChange.bind(this)}>Plain Voting platform</FormRadio>
                    </fieldset>
                  </Col>
                  </Row>
                  <Button style={{marginBottom:'10px'}} theme="accent" onClick={this.onModeSubmit.bind(this)}>Update</Button>
                </ListGroupItem>
              </ListGroup>              
              </Form>
          </Card>

     </div>
    );
  }
}