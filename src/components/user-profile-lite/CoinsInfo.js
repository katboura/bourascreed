import React from "react";
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  Button
} from "shards-react";
import RangeDatePicker from "../common/RangeDatePicker";

export default class UserAccountDetails extends React.Component {

  constructor(props) {
    super(props);
    this.state = {user: this.props.user};
  }

  componentDidUpdate(oldProps) {
    // const newProps = this.props
    // if(oldProps.user !== newProps.user) {
    //   this.setState({ user: newProps.user });
    // }
  }

  onSubmit(){
    //this function will call the service to update the user info    

    let startdateValue = document.getElementById('startDate').value;
    let endDateValue = document.getElementById('endDate').value;
    let NoOfcoinsValue = document.getElementById('NoOfcoins').value;
    
    console.log('button pressed to change coins these are the values: startdate: '
    ,startdateValue,' , endDate: ', endDateValue, ' , coins: ', NoOfcoinsValue)
    
    //we can refresh after service call and fetch the new values
    // window.location.reload();
  }


  render() {
    return (
      <div>
        <Card small className="mb-4">
        <CardHeader className="border-bottom">
          <h6 className="m-0">Coins Renewal</h6>
        </CardHeader>
        <ListGroup flush>
          <ListGroupItem className="p-3">
            <Row>
              <Col>
                <Form>
                  <Row form>
                    {/* City */}
                    <Col md="12" className="form-group">
                      <label>Coins Date Range</label>
                        <RangeDatePicker />
                    </Col>
                    <Col md="12" className="form-group">
                      <label htmlFor="NoOfcoins">Number of coins</label>
                      <FormInput
                        id="NoOfcoins"
                        type="number"
                        placeholder="Number of coins"
                        onChange={this.handleChange}
                      />
                    </Col>
                  </Row>
                  <Button theme="accent" onClick={this.onSubmit.bind(this)}>Update</Button>
                </Form>
              </Col>
            </Row>
          </ListGroupItem>
        </ListGroup>
      </Card>
  </div>
  )
}
};
