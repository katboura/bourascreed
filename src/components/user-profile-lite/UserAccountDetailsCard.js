import React from "react";
import {
  Card,
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form
} from "shards-react";

export default class UserAccountDetailsCard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {user: this.props.user};
  }

  componentDidUpdate(oldProps) {
    const newProps = this.props
    if(oldProps.user !== newProps.user) {
      this.setState({ user: newProps.user });
    }
  }

  render() {
   
    return (
      <div>
      { this.state.user ?
        <Card small className="mb-4">
        <ListGroup flush>
          <ListGroupItem className="p-3">
            <Row>
              <Col>
                <Form>
                  <Row form>
                    {/* department */}
                    <Col md="12" className="form-group">
                      <label htmlFor="fedepartmentPosition">Position</label>
                      {this.state.user.jobTitle?<div>{this.state.user.jobTitle.description}</div>:null}
                    </Col>
                    {/* role */}
                    <Col md="12" className="form-group">
                      <label htmlFor="feRole">Role</label>
                      {this.state.user.role ? <div>{this.state.user.role.description}</div> : null}
                    </Col>
                  </Row>
                
                  <Row form>
                    {/* badge */}
                    <Col md="12" className="form-group">
                      <label htmlFor="feBadge">User Badge</label>
                      {this.state.user.currentBadge ? <div>{this.state.user.currentBadge.badgeName}</div>:null}
                    </Col>
                    <Col md="12" className="form-group">
                      <label htmlFor="feBadge">Next User Badge</label>
                      {this.state.user.nextBadge ? <div>{this.state.user.nextBadge.badgeName}</div>: null}
                    </Col>
                  </Row>
                </Form>
              </Col>
            </Row>
          </ListGroupItem>
        </ListGroup>
      </Card>
    : null }
  </div>
  )
}
};
