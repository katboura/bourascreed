import React from "react";
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormGroup,
  FormInput,
  FormCheckbox,
  Button
} from "shards-react";

export default class UserAccountDetails extends React.Component {

  constructor(props) {
    super(props);
    this.state = {user: this.props.user};
  }

  componentDidUpdate(oldProps) {
    const newProps = this.props
    if(oldProps.user !== newProps.user) {
      this.setState({ user: newProps.user });
    }
  }

  onSubmit(){
    //this function will call the service to update the user info    

    let departmentValue = document.getElementById('fedepartmentPosition').value;
    let roleValue = document.getElementById('feRole').value;
    let coinsFromOthersValue = document.getElementById('feCoins').value;
    let coinsToGiveValue = document.getElementById('feCoins2').value;
    let platformAccessValue = document.getElementById('fePlatformAccess').checked;
    let badgeValue = document.getElementById('feBadge').value;
    let coinsSponsorValue = document.getElementById('feCoinsSponsor') ? document.getElementById('feCoinsSponsor').value: null;
    
    console.log('button pressed to change account these are the values: department: '
    ,departmentValue,' , role: ', roleValue, ' , coins from others: ', coinsFromOthersValue,
    ' , coins to give: ',coinsToGiveValue, ' , platform access: ', platformAccessValue,
    ' , badge: ', badgeValue, ' coins for sponsor', coinsSponsorValue)

    //we can refresh after service call and fetch the new values
    // window.location.reload();
  }


  render() {
    this.requests = [];
    if (this.state.user.requests && this.state.user.requests.length>0){
      this.requests = this.state.user.requests.map((request, id) =>
      <tr key={id}>
        <td>
          <FormGroup>
            <FormCheckbox id={"confirmRequests"+request.id} className="confirmRequests" toggle small defaultChecked={request.confirmed}>{request.description}</FormCheckbox>                                    
          </FormGroup>
        </td>                               
      </tr>
      );
    } 

    return (
      <div>
      { this.state.user.value !== "nouser" ?
        <Card small className="mb-4">
        <CardHeader className="border-bottom">
          <h6 className="m-0">{this.state.user.name}</h6>
        </CardHeader>
        <ListGroup flush>
          <ListGroupItem className="p-3">
            <Row>
              <Col>
                <Form>
                  <Row form>
                    {/* department */}
                    <Col md="6" className="form-group">
                      <label htmlFor="fedepartmentPosition">Department - Team</label>
                      <FormInput
                        id="fedepartmentPosition"
                        placeholder="Department - Position"
                        defaultValue={this.state.user.metaValue}
                      />
                    </Col>
                    {/* role */}
                    <Col md="6" className="form-group">
                      <label htmlFor="feRole">Role</label>
                      <FormInput
                        id="feRole"
                        placeholder="Role"
                        defaultValue={this.state.user.role}
                      />
                    </Col>
                  </Row>
                  <Row form>
                    {/* coins from others */}
                    <Col md="6" className="form-group">
                      <label htmlFor="feCoins">Total coins from others</label>
                      <FormInput
                        type="number"
                        id="feCoins"
                        placeholder="Number of total coins"
                        defaultValue={this.state.user.totalCoinsFromOthers}
                      />
                    </Col>
                    {/* coins to give */}
                    <Col md="6" className="form-group">
                      <label htmlFor="feCoins2">Total coins to give</label>
                      <FormInput
                        type="number"
                        id="feCoins2"
                        defaultValue={this.state.user.totalCoinsToGive}
                        placeholder="Total coins to give"
                      />
                    </Col>
                  </Row>

                  {this.state.user.role === 'sponsor' ?
                   <Row form>
                   {/* coins for sponsor */}
                   <Col md="6" className="form-group">
                     <label htmlFor="feCoins">Sponsor coins</label>
                     <FormInput
                       type="number"
                       id="feCoinsSponsor"
                       placeholder="Number of sponsor coins"
                       defaultValue={this.state.user.coinsAvailablePlusOne}
                     />
                   </Col>
                 </Row>
                  :
                  null}
                 

                  {/* access to platform */}
                  <FormGroup>
                    <FormCheckbox id="fePlatformAccess" defaultChecked={this.state.user.accessToWallet}>Access to gamification platform</FormCheckbox>
                  </FormGroup>
                  
                  <Row form>
                    {/* badge */}
                    <Col md="12" className="form-group">
                      <label htmlFor="feBadge">User Badge</label>
                      <FormInput
                        id="feBadge"
                        placeholder="Badge name"
                        defaultValue={this.state.user.badgeOfuser}
                        onChange={this.handleChange}
                      />
                    </Col>
                  </Row>
                  <Button theme="accent" onClick={this.onSubmit.bind(this)}>Update</Button>
                </Form>
              </Col>
            </Row>
          </ListGroupItem>
        </ListGroup>
      </Card>
    : null }
  </div>
  )
}
};
