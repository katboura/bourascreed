import React from "react";
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
} from "shards-react";
import ButtonGroups from "../../components/components-overview/ButtonGroups";


export default class GeneralInfo extends React.Component {

  constructor(props) {
    super(props);
    this.state = {selectedbadge: 'achievingunicorn'};
  }

  componentDidMount() {
    fetch('http://www.mocky.io/v2/5d5d2e2f3300000d0057b7b7')
    .then(res => res.json())
    .then((data) => {
      this.setState({ badgesdetails: data ,
                      selectedAward : data[0].award,
                      selectedCoins : data[0].coins,
                      selectedImage : data[0].image,
                      selectedName : data[0].name
                    })  
    })
    .catch(console.log)
  }
  
  render() {
      return (
        <div>
          <Card small className="mb-4">
            <CardHeader className="border-bottom">
              <h6 className="m-0">Badges and Awards</h6>
            </CardHeader>

            <ListGroup flush>
              <ListGroupItem className="px-3">
                  <ButtonGroups badges={this.state}/>
              </ListGroupItem>
            </ListGroup>
          </Card>

     </div>
    );
  }
}