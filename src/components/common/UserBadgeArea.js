import React from "react";
import {
  Card,
  CardBody,
  Row,
  Col
} from "shards-react";
import UsersCoinAnalysis from './UsersCoinAnalysis';
import badge1 from '../../images/badges/badge1.png';
import badge2 from '../../images/badges/badge2.png';
import badge3 from '../../images/badges/badge3.png';
import badge4 from '../../images/badges/badge4.png';
import badge5 from '../../images/badges/badge5.png';
import badge6 from '../../images/badges/badge6.png';
import sponsorImg from '../../images/points-icon.png';

export default class UserBadgeArea extends React.Component {

  constructor(props) {
    super(props);

    this.state = {user: ''};
  }

  componentDidMount() {
    let userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.setState({user: this.props.user});
  }

  componentDidUpdate(oldProps) {
    const newProps = this.props
    if(oldProps.user !== newProps.user) {
      this.setState({ user: newProps.user });
    }
  }
  
  render() {

    let src = '';
    if (this.state.user && this.state.user.role.roleTitle === 'normal'){
      var badge = this.state.user.currentBadge.badgeId;      
      if (badge === 1) {
        src = badge1;
      } else if (badge === 2){
        src = badge2;
      }else if (badge === 3){
        src = badge3;
      }else if (badge === 4){
        src = badge4;
      }else if (badge === 5){
        src = badge5;
      } else {
        src = badge6;
      }
    } else if (this.state.user && this.state.user.role.roleTitle === 'sponsor') {
      src = sponsorImg;
    }
   
      return (
        
        <Card small className="h-100" style={{margin: '17px', width:'100%'}}>
        {/* <CardHeader className="border-bottom">
          <h6 className="m-0">Your personal information card</h6>
        </CardHeader> */}
        <CardBody className="pt-0">

      <Row>
      {/* Users Overview */}
        <Col lg="5" md="12" sm="12" className="mb-4">
        {this.state.user && this.state.user.role.roleTitle === 'sponsor' ? 
        <img
            style={{float: 'left',margin: '10px', height: '170px', width:'170px'}}
            src={src}
            alt="User's badge"
            width="110"
          />
          :
          <img
            style={{float: 'left',margin: '10px', height: '170px'}}
            src={src}
            alt="User's badge"
            width="110"
          />}
        {this.state.user && this.state.user.role.roleTitle === 'normal'?
        <div style={{paddingTop: '10px'}}>
          {this.state.user.role.roleTitle === 'normal' ?<div>Your badge: <span style={{'fontSize':'18px', fontWeight:'300'}}>{this.state.user.currentBadge.badgeName}</span></div>: null}
          {this.state.user.role.roleTitle === 'normal' ?<div>Next badge: <span style={{'fontSize':'18px', fontWeight:'300'}}>{this.state.user.nextBadge.badgeName}</span></div>: null}
          {this.state.user.role.roleTitle === 'normal' ?<div><span style={{'fontSize':'20px', fontWeight:'300', color: 'blue'}}>{this.state.user.coinsRemainingForNextBadge}</span> points until next badge</div>: null}
          {this.state.user.role.roleTitle === 'normal' ?<div><span style={{'fontSize':'20px', fontWeight:'300', color: 'purple'}}>{this.state.user.coinsBalance}</span> coins available to give away to a co worker</div>: null}
          {this.state.user.role.roleTitle === 'normal' ?<div><span style={{'fontSize':'20px', fontWeight:'300', color: 'orange'}}>{this.state.user.coinsReceivedFromOtherUsers}</span> coins received from others</div>: null}
        </div>
        :null}

        {this.state.user && this.state.user.role.roleTitle === 'sponsor'?
        <div style={{paddingTop: '75px'}}>
          {this.state.user.role.roleTitle === 'sponsor' ? <div><span style={{'fontSize':'20px', fontWeight:'300', color: 'green'}}>{this.state.user.coinsForUpvotingRequests}</span> coins available for +1 requests</div>: null}
          <div>
            <a href="/vote-request">You are a sponsor! Click to vote for requests.</a>
          </div>
        </div>
        : null}
        

        <div style={{clear: 'left', paddingTop: '20px'}}></div>        
          
          {this.state.user && this.state.user.role.roleTitle === 'sponsor'?
         null  
        :
          <div>
            <a href="/add-request">It is always exciting to express your ideas! Add a request for voting!</a>
          </div>
        }

        {this.state.user && this.state.user.role.roleTitle === 'normal'?
          <div>
            <a href="/my-requests">Check out your and other peoples requests.</a>
          </div>  
        :
         null
        }
        
        
        {this.state.user && this.state.user.role.roleTitle === 'normal'? 
        <div>
          <a href="/give-coins">Give away coins to a co worker.</a>
        </div>
        :
        null
       }
      </Col>
      {this.state.user && this.state.user.role.roleTitle === 'normal'? 
      <Col lg="6" md="12" sm="12" className="mb-4">
        <UsersCoinAnalysis userCategoryCoins={this.state.user.coinsReceivedPerCoinCategory}/>
      </Col>
       :
       null
      }
     </Row>

        </CardBody>
      </Card>
    );
  }
}