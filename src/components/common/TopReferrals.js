import React from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardHeader,
  CardBody,
  ListGroup,
  ListGroupItem,
  // CardFooter,
  // Row,
  // Col,
  // FormSelect
} from "shards-react";

const TopReferrals = ({ title, referralData }) => (
  <Card small>
    <CardHeader className="border-bottom">
      <h6 className="m-0">{title}</h6>
      <div className="block-handle" />
    </CardHeader>

    <CardBody className="p-0">
      <ListGroup small flush className="list-group-small">
        {referralData.map((item, idx) => (
          <ListGroupItem key={idx} className="d-flex px-3">
            <span className="text-semibold text-fiord-blue">{item.title}</span>
            <span className="ml-auto text-right text-semibold text-reagent-gray">
              {item.value}
            </span>
            <img src={item.badge} alt="" style={{width:"25px",marginLeft:"10px"}}/>
          </ListGroupItem>          
        ))}
      </ListGroup>
    </CardBody>

    {/* <CardFooter className="border-top">
      <Row> */}
        {/* Time Span */}
        {/* <Col>
          <FormSelect
            size="sm"
            value="last-week"
            style={{ maxWidth: "130px" }}
            onChange={() => {}}
          >
            <option value="last-week">Last Week</option>
            <option value="today">Today</option>
            <option value="last-month">Last Month</option>
            <option value="last-year">Last Year</option>
          </FormSelect>
        </Col> */}

        {/* View Full Report */}
        {/* <Col className="text-right view-report"> */}
          {/* eslint-disable-next-line */}
          {/* <a href="#">Full report &rarr;</a>
        </Col>
      </Row>
    </CardFooter> */}
  </Card>
);

TopReferrals.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string,
  /**
   * The referral data.
   */
  referralData: PropTypes.array
};

TopReferrals.defaultProps = {
  title: "Users with the most successfull requests",
  referralData: [
    {
      title: "Panagiotis Stravaleksis",
      badge: require("../../images/badges/badge1.png"),
      value: "274"
    },
    {
      title: "Natasa Leonti",
      badge: require("../../images/badges/badge1.png"),
      value: "260"
    },
    {
      title: "Katerina Boura",
      badge: require("../../images/badges/badge1.png"),
      value: "214"
    },
    {
      title: "Theo Bouras",
      badge: require("../../images/badges/badge1.png"),
      value: "163"
    },
    {
      title: "Spyros Papadakis",
      badge: require("../../images/badges/badge1.png"),
      value: "106"
    },
    {
      title: "Maria Papadopoulou",
      badge: require("../../images/badges/badge2.png"),
      value: "93"
    },
    {
      title: "Alex Georgopoulos",
      badge: require("../../images/badges/badge2.png"),
      value: "69"
    },
    {
      title: "Nikos Mathioudakis",
      badge: require("../../images/badges/badge3.png"),
      value: "48"
    }
  ]
};

export default TopReferrals;
