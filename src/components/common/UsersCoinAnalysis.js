import React from "react";
import PropTypes from "prop-types";

import Chart from "../../utils/chart";

class UsersCoinAnalysis extends React.Component {
  constructor(props) {
    super(props);

    this.state = {userCategoryCoins: this.props.userCategoryCoins};
    this.canvasRef = React.createRef();
  }

  componentDidMount() {
    this.setState({userCategoryCoins: this.props.userCategoryCoins});
  }

  componentDidUpdate(oldProps) {
    const newProps = this.props

    let labels = [];
    let data = [];
    for (var i = 0; i < newProps.userCategoryCoins.length; i++) {
      let item = newProps.userCategoryCoins[i];
      let label = item.coinCategory.coinCategoryName;
      let dataItem = item.coinsReceived;
      labels.push(label);
      data.push(dataItem)
    }

    const chartOptions = {
      ...{
        legend: { display: false },
        title: {
          display: true,
          text: 'Coins earned category analysis'
        },
        scales: {
          yAxes: [{
              barPercentage: 0.2
          }]
      },
      animation: {
        duration: 0
      }
      },
      ...this.props.chartOptions
    };

    if( labels.length>0 && data.length>0 ){
      const UsersCoinAnalysis = new Chart(this.canvasRef.current, {
        type: "horizontalBar",
        data: {
          labels: labels,
          datasets: [{
            label: ['No of Coins'],
            data: data,
            backgroundColor: 'rgb(255, 99, 132)',
        }]
        },
        options: chartOptions
      });
  
      // Render the chart.
      UsersCoinAnalysis.render();
    }

    if(oldProps.userCategoryCoins !== newProps.userCategoryCoins ) {
      this.setState({ userCategoryCoins: newProps.userCategoryCoins });

      console.log(newProps.userCategoryCoins)

      
    }
  }

  render() {
    // let labels = [];
    // let data = [];
    // for (var i = 0; i < this.state.userCategoryCoins.length; i++) {
    //   let item = this.state.userCategoryCoins[i];
    //   let label = item.coinCategory.coinCategoryName;
    //   let dataItem = item.coinsReceived;
    //   labels.push(label);
    //   data.push(dataItem)
    // }
    // console.log(labels)
    // console.log(data)
    
    return (
      
          <canvas
            height="120"
            ref={this.canvasRef}
            style={{ maxWidth: "100% !important" }}
          />
    );
  }
}

UsersCoinAnalysis.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string,
  /**
   * The chart dataset.
   */
  chartData: PropTypes.object,
  /**
   * The Chart.js options.
   */
  chartOptions: PropTypes.object
};

UsersCoinAnalysis.defaultProps = {
  // chartData: {
  //   labels: this.state.user.coinCategories,
  //   datasets: [{
  //     label: ['No of Coins'],
  //     data: [20, 6, 16, 35, 25, 13],
  //     backgroundColor: 'rgb(255, 99, 132)',
  // }]
  // }
};

export default UsersCoinAnalysis;
