import React from "react";
import { ButtonGroup, Button, Form, Row, Col, FormInput } from "shards-react";

export default class ButtonGroups extends React.Component {

  constructor(props) {
    super(props);
    this.state = {selectedBadge: this.props.badges.selectedbadge,
                  badgesdetails:this.props.badges.badgesdetails,
                  selectedAward : this.props.badges.selectedAward,
                  selectedCoins : this.props.badges.selectedCoins,
                  selectedImage : this.props.badges.selectedImage,
                  selectedName : this.props.badges.selectedName
                };
  }

  changeBadges(){
    //this function will call the service to update the badges info    
    let nameValue = document.getElementById('name').value;
    let coinsValue = document.getElementById('coins').value;
    let awardValue = document.getElementById('award').value;
    
    console.log('button pressed to change badges these are the values: name: '
    ,nameValue,' , coins: ', coinsValue, ' , award: ', awardValue)
    
    //we can refresh after service call and fetch the new values
    // window.location.reload();
  }

  changeBadgedetails(value) {
    this.setState({ selectedBadge: value });
    for (var i = 0; i < this.state.badgesdetails.length; i++) {
      if (value === this.state.badgesdetails[i].key) {
        this.setState({selectedAward : this.state.badgesdetails[i].award,
                      selectedCoins : this.state.badgesdetails[i].coins,
                      selectedImage : this.state.badgesdetails[i].image,
                      selectedName : this.state.badgesdetails[i].name
        });
      }
    }
  }

  componentDidUpdate(oldProps) {
    const newProps = this.props
    if(oldProps.badges !== newProps.badges) {
      this.setState({ badgesdetails: newProps.badges.badgesdetails,
                      selectedAward : newProps.badges.selectedAward,
                      selectedCoins : newProps.badges.selectedCoins,
                      selectedImage : newProps.badges.selectedImage,
                      selectedName : newProps.badges.selectedName });
    }
  }
  
  render() {
    if (this.state.badgesdetails){
      this.items = this.state.badgesdetails.map((item) =>
      <Button theme={this.state.selectedBadge === item.key ? "info" : "white"} onClick={() => this.changeBadgedetails(item.key)} key={item.key}>{item.name}</Button>
      );
    }    

    return (
    <div>
    <ButtonGroup className="mb-5">
      {this.items}
    </ButtonGroup>
    <br/>
    <Form>
      <Row form>
        {/* Name */}
        <Col md="8" className="form-group">
        <label htmlFor="name">Name</label>
          <FormInput
            id="name"
            placeholder="Badge name"
            defaultValue={this.state.selectedName}
          />
        </Col>

        <Col md="8" className="form-group">
        <label htmlFor="coins">Coins</label>
          <FormInput
            id="coins"
            type="number"
            placeholder="Coins"
            defaultValue={this.state.selectedCoins}
          />
        </Col>

        <Col md="8" className="form-group">
        <label htmlFor="award">Award</label>
          <FormInput
            id="award"
            placeholder="Award"
            defaultValue={this.state.selectedAward}
          />
        </Col>

        
      </Row>
      <Button theme="accent" onClick={this.changeBadges.bind(this)}>Update</Button>
    </Form>
  </div>
)}};