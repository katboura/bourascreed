export default function() {
  let arrayOfNavItems = [
      {
        title: "Ideas feed",
        to: "/feed",
        htmlBefore: '<i class="material-icons">view_module</i>',
        htmlAfter: ""
      },
      {
        title: "Add your idea",
        to: "/create-proposal",
        htmlBefore: '<i class="material-icons">edit</i>',
        htmlAfter: ""
      }
    ]
    return arrayOfNavItems;
  }