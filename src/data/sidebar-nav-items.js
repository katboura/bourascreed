export default function () {
  let userDetails = JSON.parse(localStorage.getItem('userDetails'));
  let arrayOfNavItems = [];

  if (userDetails && userDetails.role && userDetails.role.roleTitle ===
    'normal') {
    arrayOfNavItems = [
      {
        title: "My Profile",
        to: "/blog-overview",
        htmlBefore: '<i class="material-icons">account_circle</i>',
        htmlAfter: ""
      },
      {
        title: "Success Feed",
        to: "/success-feed",
        htmlBefore: '<i class="material-icons">view_module</i>',
      },
      {
        title: "Give away coins",
        to: "/give-coins",
        htmlBefore: '<i class="material-icons">attach_money</i>',
        htmlAfter: ""
      },
      {
        title: "Store",
        to: "/store",
        htmlBefore: '<i class="material-icons">shopping_cart</i>',
        htmlAfter: ""
      },
      {
        title: "Add An Idea",
        to: "/add-request",
        htmlBefore: '<i class="material-icons">note_add</i>',
        htmlAfter: ""
      },
      {
        title: "Ideas",
        to: "/my-requests",
        htmlBefore: '<i class="material-icons">view_module</i>',
        htmlAfter: ""
      }

    ]
  } else if (userDetails && userDetails.role && userDetails.role.roleTitle ===
    'sponsor') {
    arrayOfNavItems = [{
      title: "Dashboard",
      to: "/blog-overview",
      htmlBefore: '<i class="material-icons">edit</i>',
      htmlAfter: ""
    },
    {
      title: "Ideas to sponsor",
      to: "/vote-request",
      htmlBefore: '<i class="material-icons">view_module</i>',
      htmlAfter: ""
    }
    ]
  }
  // else if (userDetails && mode) {
  //   arrayOfNavItems = [
  //     {
  //       title: "Crowdfunding feed",
  //       to: "/feed",
  //       htmlBefore: '<i class="material-icons">view_module</i>',
  //       htmlAfter: ""
  //     },
  //     {
  //       title: "Create a proposal",
  //       to: "/create-proposal",
  //       htmlBefore: '<i class="material-icons">edit</i>',
  //       htmlAfter: ""
  //     }
  //   ]
  // }
  return arrayOfNavItems;
}

// [
//   {
//     title: "Dashboard",
//     to: "/blog-overview",
//     htmlBefore: '<i class="material-icons">edit</i>',
//     htmlAfter: ""
//   },
//   {
//     title: "Requests",
//     to: "/vote-request",
//     htmlBefore: '<i class="material-icons">view_module</i>',
//     htmlAfter: ""
//   },
//   {
//     title: "Add Request",
//     to: "/add-request",
//     htmlBefore: '<i class="material-icons">note_add</i>',
//     htmlAfter: ""
//   },
//   {
//     title: "Give away coins",
//     to: "/give-coins",
//     htmlBefore: '<i class="material-icons">attach_money</i>',
//     htmlAfter: ""
//   },
// {
//   title: "General Configuration",
//   htmlBefore: '<i class="material-icons">note_add</i>',
//   to: "/general-configuration",
// },
// {
//   title: "User Configuration",
//   htmlBefore: '<i class="material-icons">person</i>',
//   to: "/users-profiles",
// },
// {
//   title: "Forms & Components",
//   htmlBefore: '<i class="material-icons">view_module</i>',
//   to: "/components-overview",
// },
// {
//   title: "Blog Posts",
//   htmlBefore: '<i class="material-icons">vertical_split</i>',
//   to: "/blog-posts",
// },


// {
//   title: "Tables",
//   htmlBefore: '<i class="material-icons">table_chart</i>',
//   to: "/tables",
// },

// {
//   title: "Errors",
//   htmlBefore: '<i class="material-icons">error</i>',
//   to: "/errors",
// }
